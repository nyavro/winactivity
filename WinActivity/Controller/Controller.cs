using System;
using System.Collections.Generic;
using System.Text;
using WinActivityApplication.Views;
using WinActivityApplication.Model;

namespace WinActivityApplication.Controller
{
    public class EventsController: IController
    {
        public EventsController(IModel model)
        {
            model_ = model;
        }

        public void setActiveView(IView view)
        {
            model_.removeObserver(activeView_);
            activeView_ = view;
            model_.addObserver(activeView_);
        }

        private IView activeView_;
        private IModel model_;        
    }
}
