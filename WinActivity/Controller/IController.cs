using System;
using System.Collections.Generic;
using System.Text;
using WinActivityApplication.Views;

namespace WinActivityApplication.Controller
{
    public interface IController
    {
        void setActiveView(IView activeView);

        void setDateRange(DateRange activeDateRange);

        void update();
    }
}
