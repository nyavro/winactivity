using System;
using System.Collections.Generic;
using System.Text;
using WinActivityApplication.Views;
using WinActivityApplication.Model;
using WinActivityApplication.Events;

namespace WinActivityApplication.Controller
{
    public class EventsController: IController
    {
        public EventsController(IModel model, IEventsProcessorProxy proxy, IEventsProcessor processor)
        {
            model_ = model;            
            processor_ = processor;
            proxy_ = proxy;
        }

        public void setActiveView(IView view)
        {
            model_.removeObserver(activeView_);
            activeView_ = view;
            model_.addObserver(activeView_);
            model_.update(true);
        }

        public void setDateRange(DateRange activeDateRange)
        {
            if (activeDateRange.contains(DateTime.Today))
            {
                proxy_.addObserver(processor_);
            }
            else
            {
                proxy_.removeObserver(processor_);
            }
            model_.load(activeDateRange);
        }

        public void update()
        {
            model_.update(false);
        }

        private IView activeView_;
        private IModel model_;
        private IEventsProcessorProxy proxy_;
        private IEventsProcessor processor_;
    }
}
