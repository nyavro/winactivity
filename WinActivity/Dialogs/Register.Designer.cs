namespace WinActivityApplication.Dialogs
{
    partial class Register
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.serialNumberTextBox = new System.Windows.Forms.TextBox();
            this.serialNumberLabel = new System.Windows.Forms.Label();
            this.registerButton = new System.Windows.Forms.Button();
            this.cancelButton = new System.Windows.Forms.Button();
            this.continueTrialButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // serialNumberTextBox
            // 
            this.serialNumberTextBox.Location = new System.Drawing.Point(15, 72);
            this.serialNumberTextBox.Multiline = true;
            this.serialNumberTextBox.Name = "serialNumberTextBox";
            this.serialNumberTextBox.Size = new System.Drawing.Size(307, 182);
            this.serialNumberTextBox.TabIndex = 2;
            // 
            // serialNumberLabel
            // 
            this.serialNumberLabel.AutoEllipsis = true;
            this.serialNumberLabel.Location = new System.Drawing.Point(12, 9);
            this.serialNumberLabel.Name = "serialNumberLabel";
            this.serialNumberLabel.Size = new System.Drawing.Size(307, 52);
            this.serialNumberLabel.TabIndex = 3;
            this.serialNumberLabel.Text = "serial";
            // 
            // registerButton
            // 
            this.registerButton.Location = new System.Drawing.Point(15, 260);
            this.registerButton.Name = "registerButton";
            this.registerButton.Size = new System.Drawing.Size(307, 23);
            this.registerButton.TabIndex = 4;
            this.registerButton.Text = "Register";
            this.registerButton.UseVisualStyleBackColor = true;
            this.registerButton.Click += new System.EventHandler(this.registerButton_Click);
            // 
            // cancelButton
            // 
            this.cancelButton.Location = new System.Drawing.Point(176, 289);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(146, 23);
            this.cancelButton.TabIndex = 5;
            this.cancelButton.Text = "Cancel";
            this.cancelButton.UseVisualStyleBackColor = true;
            this.cancelButton.Click += new System.EventHandler(this.cancelButton_Click);
            // 
            // continueTrialButton
            // 
            this.continueTrialButton.Location = new System.Drawing.Point(15, 289);
            this.continueTrialButton.Name = "continueTrialButton";
            this.continueTrialButton.Size = new System.Drawing.Size(155, 23);
            this.continueTrialButton.TabIndex = 6;
            this.continueTrialButton.Text = "Continue trial";
            this.continueTrialButton.UseVisualStyleBackColor = true;
            this.continueTrialButton.Click += new System.EventHandler(this.continueTrialButton_Click);
            // 
            // Register
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(336, 324);
            this.Controls.Add(this.continueTrialButton);
            this.Controls.Add(this.cancelButton);
            this.Controls.Add(this.registerButton);
            this.Controls.Add(this.serialNumberLabel);
            this.Controls.Add(this.serialNumberTextBox);
            this.Name = "Register";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Register";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox serialNumberTextBox;
        private System.Windows.Forms.Label serialNumberLabel;
        private System.Windows.Forms.Button registerButton;
        private System.Windows.Forms.Button cancelButton;
        private System.Windows.Forms.Button continueTrialButton;
    }
}