using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using WinActivityApplication.Controller;
using WinActivityApplication.Dialogs;
using WinActivityApplication.Modules;

namespace WinActivityApplication
{
    public partial class InvisibleForm : Form
    {
        public InvisibleForm(IServerApplication serverApplication)
        {
            serverApplication_ = serverApplication;           
            InitializeComponent();

            showMainForm();
        }

        public void showMainForm()
        {
            clientApplication_ = new ClientApplication(serverApplication_);
            form_ = new MainForm(this, clientApplication_);
            //form.Visible = true;
            form_.SuspendLayout();
            form_.Show();
            form_.WindowState = FormWindowState.Normal;
            form_.ResumeLayout();
            form_.Invalidate();
        }

        public void hideMainForm()
        {            
            form_.Dispose();
            form_ = null;
        }

        private bool isMainFormVisible()
        {
            return form_ != null;
        }

        private void notifyIcon_Click(object sender, System.EventArgs e)
        {
            if (isMainFormVisible())
            {
                hideMainForm();
            }
            else
            {
                showMainForm();
            }
            /*
            if (WindowState == FormWindowState.Minimized)
            {
                Show();
                WindowState = FormWindowState.Normal;
                ResumeLayout();
                Invalidate();
            }
            else
            {
                SuspendLayout();
                WindowState = FormWindowState.Minimized;
            }*/
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Dispose();
        }

        private MainForm form_;        
        private IServerApplication serverApplication_;
        private IClientApplication clientApplication_;                
    }
}