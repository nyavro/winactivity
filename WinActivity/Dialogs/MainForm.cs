using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Threading;
using System.Globalization;
using WinActivityApplication.Events;
using WinActivityApplication.utils;
using WinActivityApplication.Database;
using WinActivityApplication.Dialogs;
using WinActivityApplication.Controller;
using WinActivityApplication.Views;
using WinActivityApplication.Model;
using WinActivityApplication.Modules;

namespace WinActivityApplication.Dialogs
{
    public partial class MainForm : Form
    {
        public MainForm(Control owner, IClientApplication clientApplication)
        {
            owner_ = owner;
            clientApplication_ = clientApplication;

            manager_ = new IconsManager();
            contextMenu_ = new ContextMenu();
            InitializeComponent();
        }

        public MainForm(IClientApplication clientApplication) : this(null, clientApplication)
        {
        }
        
        private delegate void initTreeCallback();

        private void Init(object sender, System.EventArgs e)
        {
            initTree();
            contextMenu_.MenuItems.Add(new MenuItem());
            manager_.addImage(InactivityEvent.getName(), GraphicsUtils.convert(WinActivityApplication.Properties.Resources.InactivityIcon));
            manager_.init(DBManager.getReader().getApplicationsIcons()); 
            
            activityStatisticView_ = new ActivityStatisticView(statisticView, manager_);            
            activityLogView_ = new ActivityLogView(logList, manager_);
            infoMultiView_ = new MultiView();
            infoMultiView_.add(new InfoView(infoPanel));
            infoMultiView_.add(new TimelineView(timeline, manager_));                        

            activeDateRange_ = new DateRange(DateTimeUtil.getDayStart(DateTime.Today), DateTimeUtil.getDayEnd(DateTime.Today));

            clientApplication_.getController().setActiveView(activityStatisticView_);
            clientApplication_.getController().setDateRange(activeDateRange_);            
        }                        

        private void initTree()
        {
            if (historyTree.InvokeRequired)
            {
                initTreeCallback cb = new initTreeCallback(initTree);
                this.Invoke(cb);
            }
            else
            {
                historyTree.Nodes.Clear();
                TreeNode node;
                LinkedList<DateTime> dates = DBManager.getReader().getDatesWithEvents(
                    DateTimeUtil.getLastHalfYear().Last.Value,
                    DateTimeUtil.getToday().Last.Value
                );

                //today            
                addRange(WinActivityApplication.Properties.i18n.Today, DateTimeUtil.getToday());

                if (historyTree.Nodes.Count > 0)
                {
                    node = historyTree.Nodes[0];
                    historyTree.SelectedNode = node.Nodes[0];
                    node.Nodes[0].BackColor = Color.Aqua;
                }

                //yesterday

                addRange(WinActivityApplication.Properties.i18n.Yesterday, DateTimeUtil.getYesterday(), dates);

                //last week            
                addRange(WinActivityApplication.Properties.i18n.LastWeek, DateTimeUtil.getLastWeek(), dates);

                //this month            
                addRange(WinActivityApplication.Properties.i18n.ThisMonth, DateTimeUtil.getThisMonth(), dates);

                //previous months
                addLastHalfYear(dates);
            }
        }

        private void addLastHalfYear(LinkedList<DateTime> daysWithEvents)
        {
            foreach (DateTime item in DateTimeUtil.getLastHalfYear())
            {                
                addRange(CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(item.Month), DateTimeUtil.getMonth(item.Year, item.Month), daysWithEvents);
            }
        }

        private void addRange(string caption, LinkedList<DateTime> range)
        {
            addRange(caption, range, null);
        }

        private void addRange(string caption, LinkedList<DateTime> range, LinkedList<DateTime> daysWithEvents)
        {
            TreeNode node = new TreeNode(caption);
            
            foreach (DateTime date in range)
            {
                DateTime start = DateTimeUtil.getDayStart(date);
                if (daysWithEvents!=null && !daysWithEvents.Contains(start))
                {                    
                    //day.Text += " no data";
                    continue;
                }
                TreeNode day = new TreeNode(date.ToString("d", CultureInfo.CurrentCulture));
                //date.ToString(WinActivityApplication.Properties.Options.DateFormat));
                node.Nodes.Insert(0, day);
                if (date.DayOfWeek == DayOfWeek.Sunday || date.DayOfWeek == DayOfWeek.Saturday)
                {
                    day.BackColor = Color.LightGreen;
                }
                else
                {
                    day.BackColor = Color.FromArgb(250, 250, 250);
                }
                day.Tag = date;                
            }
            if (node.Nodes.Count != 0)
            {
                historyTree.Nodes.Add(node);
            }
        }

        private void historyTree_NodeMouseClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            if (e.Node.Nodes.Count == 0 && e.Node.Tag!=null)
            {
                DateTime date = (DateTime)e.Node.Tag;
                setActiveViewDate(date);
            }
        }

        private void setActiveViewDate(DateTime date)
        {
            activeDateRange_ = new DateRange(DateTimeUtil.getDayStart(date), DateTimeUtil.getDayEnd(date));
            clientApplication_.getController().setDateRange(activeDateRange_); 
        }

        private void tabControl_SelectedIndexChanged(object sender, EventArgs e)
        {            
            TabControl control = (TabControl)sender;
            
            IView activeView;
            if (control.SelectedTab == tabLog)
            {
                activeView = activityLogView_;                
            }
            else if (control.SelectedTab == tabStatistic)
            {
                activeView = activityStatisticView_;
            }
            else if (control.SelectedTab == tabInfo)
            {
                activeView = infoMultiView_;
            }
            else 
            {
                return;
            }
            clientApplication_.getController().setActiveView(activeView);            
        }                

        private void menuItemExit_Click(object sender, EventArgs e)
        {
            if (owner_ != null)
            {
                owner_.Dispose();
            }
        }

        private void menuItemAboutBox_Click(object sender, EventArgs e)
        {
            AboutBox aboutBox = new AboutBox();
            aboutBox.ShowDialog(this);
        }

        private void MainForm_Resize(object sender, EventArgs e)
        {
            if (FormWindowState.Minimized == WindowState)
            {
                SuspendLayout();
                Hide();
            }            
        }

        private void MainForm_ResizeBegin(object sender, EventArgs e)
        {
            SuspendLayout();
        }

        private void MainForm_ResizeEnd(object sender, EventArgs e)
        {
            ResumeLayout();
            Invalidate();
        }

        private void timeline_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                contextMenu_.Show(this, new Point(e.X, e.Y));
            }
        }

        private void statisticView_ColumnClick(object sender, ColumnClickEventArgs e)
        {
            ((ActivityStatisticView)activityStatisticView_).sort(e.Column);
        }

        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            Console.WriteLine("form closing");
        }  

        private IView activityStatisticView_;
        private IView activityLogView_;        
        private IconsManager manager_;
        private MultiView infoMultiView_;
        private ContextMenu contextMenu_;
        private IClientApplication clientApplication_;
        private Control owner_;
    }
}