using System.Windows.Forms;
using System.Resources;
using System.Reflection;
using System;
using System.Drawing;
using System.Collections.Generic;
using System.Globalization;
using System.Threading;
using WinActivityApplication.Events;
using WinActivityApplication.Controls;
namespace WinActivityApplication.Dialogs
{
    partial class MainForm 
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;
                      

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            clientApplication_.destroy();
            if (disposing && (components != null))
            {                
                components.Dispose();
            }
            base.Dispose(disposing);
        }       

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.historyTree = new System.Windows.Forms.TreeView();
            this.tabControl = new System.Windows.Forms.TabControl();
            this.tabStatistic = new System.Windows.Forms.TabPage();
            this.statisticView = new System.Windows.Forms.ListView();
            this.processId = new System.Windows.Forms.ColumnHeader();
            this.totalDuration = new System.Windows.Forms.ColumnHeader();
            this.percentage = new System.Windows.Forms.ColumnHeader();
            this.tabInfo = new System.Windows.Forms.TabPage();
            this.infoPanel = new WinActivityApplication.Controls.InfoPanel();
            this.timeline = new WinActivityApplication.Controls.Timeline();
            this.tabLog = new System.Windows.Forms.TabPage();
            this.logList = new System.Windows.Forms.ListView();
            this.applicationTitle = new System.Windows.Forms.ColumnHeader();
            this.processName = new System.Windows.Forms.ColumnHeader();
            this.start = new System.Windows.Forms.ColumnHeader();
            this.duration = new System.Windows.Forms.ColumnHeader();
            this.mainMenu = new System.Windows.Forms.MainMenu(this.components);
            this.fileMenuItem = new System.Windows.Forms.MenuItem();
            this.exitMenuItem = new System.Windows.Forms.MenuItem();
            this.helpMenuItem = new System.Windows.Forms.MenuItem();
            this.aboutMenuItem = new System.Windows.Forms.MenuItem();
            this.tabControl.SuspendLayout();
            this.tabStatistic.SuspendLayout();
            this.tabInfo.SuspendLayout();
            this.tabLog.SuspendLayout();
            this.SuspendLayout();
            // 
            // historyTree
            // 
            this.historyTree.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)));
            this.historyTree.Location = new System.Drawing.Point(12, 2);
            this.historyTree.Name = "historyTree";
            this.historyTree.Size = new System.Drawing.Size(215, 372);
            this.historyTree.TabIndex = 0;
            this.historyTree.NodeMouseClick += new System.Windows.Forms.TreeNodeMouseClickEventHandler(this.historyTree_NodeMouseClick);
            // 
            // tabControl
            // 
            this.tabControl.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl.Controls.Add(this.tabStatistic);
            this.tabControl.Controls.Add(this.tabInfo);
            this.tabControl.Controls.Add(this.tabLog);
            this.tabControl.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.tabControl.Location = new System.Drawing.Point(233, 2);
            this.tabControl.Multiline = true;
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 0;
            this.tabControl.Size = new System.Drawing.Size(532, 372);
            this.tabControl.TabIndex = 1;
            this.tabControl.SelectedIndexChanged += new System.EventHandler(this.tabControl_SelectedIndexChanged);
            // 
            // tabStatistic
            // 
            this.tabStatistic.Controls.Add(this.statisticView);
            this.tabStatistic.Location = new System.Drawing.Point(4, 22);
            this.tabStatistic.Name = "tabStatistic";
            this.tabStatistic.Padding = new System.Windows.Forms.Padding(3);
            this.tabStatistic.Size = new System.Drawing.Size(524, 346);
            this.tabStatistic.TabIndex = 0;
            this.tabStatistic.Text = global::WinActivityApplication.Properties.i18n.StatisticViewCaption;
            this.tabStatistic.UseVisualStyleBackColor = true;
            // 
            // statisticView
            // 
            this.statisticView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.statisticView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.processId,
            this.totalDuration,
            this.percentage});
            this.statisticView.FullRowSelect = true;
            this.statisticView.Location = new System.Drawing.Point(6, 6);
            this.statisticView.MultiSelect = false;
            this.statisticView.Name = "statisticView";
            this.statisticView.Size = new System.Drawing.Size(518, 337);
            this.statisticView.TabIndex = 0;
            this.statisticView.UseCompatibleStateImageBehavior = false;
            this.statisticView.View = System.Windows.Forms.View.Details;
            this.statisticView.ColumnClick += new System.Windows.Forms.ColumnClickEventHandler(this.statisticView_ColumnClick);
            // 
            // processId
            // 
            this.processId.Text = global::WinActivityApplication.Properties.i18n.ProcessNameColumnHeader;
            this.processId.Width = 254;
            // 
            // totalDuration
            // 
            this.totalDuration.Text = global::WinActivityApplication.Properties.i18n.TotalDurationColumnHeader;
            this.totalDuration.Width = 142;
            // 
            // percentage
            // 
            this.percentage.Text = global::WinActivityApplication.Properties.i18n.PercentageColumnHeader;
            this.percentage.Width = 116;
            // 
            // tabInfo
            // 
            this.tabInfo.Controls.Add(this.infoPanel);
            this.tabInfo.Controls.Add(this.timeline);
            this.tabInfo.Location = new System.Drawing.Point(4, 22);
            this.tabInfo.Name = "tabInfo";
            this.tabInfo.Padding = new System.Windows.Forms.Padding(3);
            this.tabInfo.Size = new System.Drawing.Size(524, 346);
            this.tabInfo.TabIndex = 2;
            this.tabInfo.Text = global::WinActivityApplication.Properties.i18n.InfoViewCaption;
            this.tabInfo.UseVisualStyleBackColor = true;
            // 
            // infoPanel
            // 
            this.infoPanel.Location = new System.Drawing.Point(0, 0);
            this.infoPanel.Name = "infoPanel";
            this.infoPanel.Size = new System.Drawing.Size(520, 82);
            this.infoPanel.TabIndex = 0;
            // 
            // timeline
            // 
            this.timeline.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.timeline.Location = new System.Drawing.Point(0, 82);
            this.timeline.Name = "timeline";
            this.timeline.Size = new System.Drawing.Size(520, 82);
            this.timeline.TabIndex = 0;
            this.timeline.MouseDown += new System.Windows.Forms.MouseEventHandler(this.timeline_MouseDown);
            // 
            // tabLog
            // 
            this.tabLog.Controls.Add(this.logList);
            this.tabLog.Location = new System.Drawing.Point(4, 22);
            this.tabLog.Name = "tabLog";
            this.tabLog.Padding = new System.Windows.Forms.Padding(3);
            this.tabLog.Size = new System.Drawing.Size(524, 346);
            this.tabLog.TabIndex = 1;
            this.tabLog.Text = global::WinActivityApplication.Properties.i18n.LogViewCaption;
            this.tabLog.UseVisualStyleBackColor = true;
            // 
            // logList
            // 
            this.logList.Activation = System.Windows.Forms.ItemActivation.OneClick;
            this.logList.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.logList.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.applicationTitle,
            this.processName,
            this.start,
            this.duration});
            this.logList.FullRowSelect = true;
            this.logList.GridLines = true;
            this.logList.HideSelection = false;
            this.logList.Location = new System.Drawing.Point(3, 3);
            this.logList.Name = "logList";
            this.logList.Size = new System.Drawing.Size(523, 381);
            this.logList.TabIndex = 0;
            this.logList.UseCompatibleStateImageBehavior = false;
            this.logList.View = System.Windows.Forms.View.Details;
            // 
            // applicationTitle
            // 
            this.applicationTitle.Text = global::WinActivityApplication.Properties.i18n.ApplicationTitleColumnHeader;
            this.applicationTitle.Width = 260;
            // 
            // processName
            // 
            this.processName.Text = global::WinActivityApplication.Properties.i18n.ProcessNameColumnHeader;
            this.processName.Width = 85;
            // 
            // start
            // 
            this.start.Text = global::WinActivityApplication.Properties.i18n.StartColumnHeader;
            this.start.Width = 85;
            // 
            // duration
            // 
            this.duration.Text = global::WinActivityApplication.Properties.i18n.DurationColumnHeader;
            this.duration.Width = 85;
            // 
            // mainMenu
            // 
            this.mainMenu.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.fileMenuItem,
            this.helpMenuItem});
            // 
            // fileMenuItem
            // 
            this.fileMenuItem.Index = 0;
            this.fileMenuItem.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.exitMenuItem});
            this.fileMenuItem.Text = global::WinActivityApplication.Properties.i18n.FileMenuItemText;
            // 
            // exitMenuItem
            // 
            this.exitMenuItem.Index = 0;
            this.exitMenuItem.Text = global::WinActivityApplication.Properties.i18n.ExitMenuItemText;
            this.exitMenuItem.Click += new System.EventHandler(this.menuItemExit_Click);
            // 
            // helpMenuItem
            // 
            this.helpMenuItem.Index = 1;
            this.helpMenuItem.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.aboutMenuItem});
            this.helpMenuItem.Text = global::WinActivityApplication.Properties.i18n.HelpMenuItemText;
            // 
            // aboutMenuItem
            // 
            this.aboutMenuItem.Index = 0;
            this.aboutMenuItem.Text = global::WinActivityApplication.Properties.i18n.AboutMenuItemText;
            this.aboutMenuItem.Click += new System.EventHandler(this.menuItemAboutBox_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(777, 420);
            this.Controls.Add(this.tabControl);
            this.Controls.Add(this.historyTree);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Menu = this.mainMenu;
            this.Name = "MainForm";
            this.Text = "Windows Activity Watcher";
            this.Load += new System.EventHandler(this.Init);
            this.ResizeBegin += new System.EventHandler(this.MainForm_ResizeBegin);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
            this.Resize += new System.EventHandler(this.MainForm_Resize);
            this.ResizeEnd += new System.EventHandler(this.MainForm_ResizeEnd);
            this.tabControl.ResumeLayout(false);
            this.tabStatistic.ResumeLayout(false);
            this.tabInfo.ResumeLayout(false);
            this.tabLog.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        

        private TreeView historyTree;
        private TabControl tabControl;
        private TabPage tabLog;
        private TabPage tabStatistic;
        private ListView logList;
        private ListView statisticView;
        private ColumnHeader applicationTitle;
        private ColumnHeader duration;
        private ColumnHeader start;
        private ColumnHeader processName;        
        private ColumnHeader processId;
        private ColumnHeader totalDuration;
        private DateRange activeDateRange_;
        private ColumnHeader percentage;
        private MainMenu mainMenu;
        private MenuItem fileMenuItem;
        private MenuItem exitMenuItem;
        private MenuItem helpMenuItem;
        private MenuItem aboutMenuItem;
        private TabPage tabInfo;
        private InfoPanel infoPanel;
        private Timeline timeline;
    }
}

