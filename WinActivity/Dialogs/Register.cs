using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using WinActivityApplication.Security;
using System.Threading;
using System.IO;

namespace WinActivityApplication.Dialogs
{
    public partial class Register : Form
    {
        public Register(bool allowSkip, RegistrationState state)
        {
            InitializeComponent();
            Text = Properties.i18n.RegisterCaption;
            serialNumberLabel.Text = getStateMessage(state);
            registerButton.Text = Properties.i18n.RegisterButtonText;
            cancelButton.Text = Properties.i18n.CancelButtonText;
            continueTrialButton.Text = Properties.i18n.ContinueTrialButtonText;
            continueTrialButton.Enabled = allowSkip;
        }

        private string getStateMessage(RegistrationState state)
        {
            LicenseInfo license = Program.getLicenseInfo();
            switch (state)
            {
                case RegistrationState.full:
                {                   
                    string strExpiration = Utils.getShortDate(license.getVersionExpirationDate());
                    return string.Format(Properties.i18n.FullVersionMessage, strExpiration);
                }
                case RegistrationState.fullVersionExpired:
                {
                    string strExpiration = Utils.getShortDate(license.getVersionExpirationDate());                   
                    return string.Format(Properties.i18n.RegisterFullVersionExpired, strExpiration);
                }
                case RegistrationState.invalid: return Properties.i18n.InvalidLicenseMessage;
                case RegistrationState.trial:   return Properties.i18n.RegisterTrialMessage;
                case RegistrationState.trialExpired: return Properties.i18n.RegisterTrialExpiredMessage;
            }

            return "";
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            isCancelled_ = true;
            Dispose();
        }

        private void registerButton_Click(object sender, EventArgs e)
        {
            isCancelled_ = false;
            licenseXML_ = serialNumberTextBox.Text;
            Dispose();
        }   

        public bool isCancelled()
        {
            return isCancelled_;
        }

        public bool isContinueTrial()
        {
            return isContinueTrial_;
        }

        private void continueTrialButton_Click(object sender, EventArgs e)
        {
            isContinueTrial_ = true;
            isCancelled_ = false;
            Dispose();
        }

        public string getLicenseXML()
        {
            return licenseXML_;
        }    

        private bool isCancelled_ = true;
        private bool isContinueTrial_ = false;
        private string licenseXML_;
    }
}