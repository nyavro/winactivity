using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.Globalization;

namespace WinActivityApplication
{
    enum ListMode
    {
        staticMode,
        dynamicMode
    }

    class ActivityLogView : ListView, IView
    {
        private ListViewItem.ListViewSubItem updatableItem;

        public void setItems(LinkedList<ActivityEvent> events)
        {            
            Items.Clear();
            foreach (ActivityEvent activityEvent in events) 
            {
                ListViewItem listViewItem = new ListViewItem(activityEvent.getTitle());
                listViewItem.SubItems.Add(new ListViewItem.ListViewSubItem(listViewItem, activityEvent.getProcessName()));
                listViewItem.SubItems.Add(new ListViewItem.ListViewSubItem(listViewItem, activityEvent.getStart().ToString("T", CultureInfo.CurrentCulture)));
                listViewItem.SubItems.Add(new ListViewItem.ListViewSubItem(listViewItem, Utils.getTimeSpanString(activityEvent.getDuration())));

                Items.Insert(0, listViewItem);
            }
        }

        public void setMode(ListMode value)
        {
            mode = value;
        }

        public void setActiveItemText(string text)
        {
            if (mode==ListMode.dynamicMode)
            {
                updatableItem.Text = text;
            }
        }

        public void createActiveItem(IEvent newEvent, int imageIndex)
        {
            if (mode==ListMode.staticMode)
            {
                return;
            }
            ListViewItem listViewItem = new ListViewItem(newEvent.getTitle(), imageIndex);
            listViewItem.SubItems.Add(new ListViewItem.ListViewSubItem(listViewItem, newEvent.getProcessName()));
            listViewItem.SubItems.Add(new ListViewItem.ListViewSubItem(listViewItem, newEvent.getStart().ToString("T", CultureInfo.CurrentCulture)));
            updatableItem = new ListViewItem.ListViewSubItem(listViewItem, Utils.getTimeSpanString(TimeSpan.FromTicks(0)));
            listViewItem.SubItems.Add(updatableItem);

            Items.Insert(0, listViewItem);
        }

        public void setDate(DateTime date)
        {
            if (date.Equals(DateTime.Today))
            {
                setMode(ListMode.dynamicMode);
            }
            else
            {
                setMode(ListMode.staticMode);
            }

            LinkedList<ActivityEvent> events = DBManager.getInstance().getEventsByDateRange(Utils.getDayStart(date), Utils.getDayEnd(date));
            setItems(events);
        }
        
        private ListMode mode = ListMode.dynamicMode;
    }
}
