using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;

namespace WinActivityApplication
{
    public class ProcessDescriptor
    {
        private ProcessDescriptor(IntPtr hwnd, ApplicationsDBCache cache)
        {
            IntPtr id = hwnd;
            string path = APIUtils.getApplicationPath(id);
            Icon icon = APIUtils.getSmallIcon(path);

            title_ = APIUtils.getApplicationTitle(id);                        
            name_ = APIUtils.getApplicationName(path);            
            
            cache.saveIcon(name_, icon);
        }

        internal static ProcessDescriptor getAcitveProcess(ApplicationsDBCache cache)
        {
            return new ProcessDescriptor(APIUtils.getForegroundWindow(), cache);
        }

        public string getApplicationName()
        {
            return name_;
        }

        public string getTitle()
        {
            return title_;
        }

        private string name_;
        private string title_;

        
    }
}
