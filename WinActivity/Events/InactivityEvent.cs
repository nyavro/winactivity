using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;

namespace WinActivityApplication.Events
{
    [Serializable]
    class InactivityEvent : AbstractEvent
    {
        public override string getTitle()
        {
            return WinActivityApplication.Properties.i18n.InactivityEventTitle;
        }        

        public override string getApplicationName()
        {
            return getName();
        }

        public static string getName()
        {
            return WinActivityApplication.Properties.i18n.InactivityEventName;
        }
    }
}
