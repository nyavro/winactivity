using System;
using System.Collections.Generic;
using System.Text;
using WinActivityApplication.Events;
using WinActivityApplication.Database;

namespace WinActivityApplication.Events
{
    public class EventsBuffer : IEventsProcessor
    {
        public EventsBuffer(IDBWriter storage)
        {
            storage_ = storage;
        }

        public void processEvent(IEvent newEvent)
        {
            if (currentEvent_ != null)
            {
                storage_.save(currentEvent_);
            }
            currentEvent_ = newEvent;
        }       

        private IEvent currentEvent_;
        private IDBWriter storage_;
    }    
}
