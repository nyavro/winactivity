using System;
using System.Collections.Generic;
using System.Text;

namespace WinActivityApplication.Events
{
    [Serializable]
    public class ApplicationStartup: AbstractEvent
    {
        public override string getTitle()
        {
            return WinActivityApplication.Properties.i18n.ApplicationStartupEventTitle;
        }

        public override string getApplicationName()
        {
            return WinActivityApplication.Properties.i18n.ApplicationStartupEventTitle;
        }

        public override TimeSpan getDuration()
        {
            return new TimeSpan();
        }
    }
}
