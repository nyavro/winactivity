using System;
using System.Collections.Generic;
using System.Text;

namespace WinActivityApplication.Events
{
    public class EventsProcessorProxy : IEventsProcessorProxy
    {
        public void addObserver(IEventsProcessor observer)
        {
            lock (this)
            {
                if (!observers_.Contains(observer))
                {
                    observers_.Add(observer);
                }
            }
        }

        public void removeObserver(IEventsProcessor observer)
        {
            lock (this)
            {
                observers_.Remove(observer);
            }
        }

        public void notifyObservers(IEvent newEvent)
        {
            ICollection<IEventsProcessor> observers = new LinkedList<IEventsProcessor>();
            lock (this)
            {
                foreach (IEventsProcessor observer in observers_)
                {
                    observers.Add(observer);
                }
            }

            foreach (IEventsProcessor observer in observers)
            {
                observer.processEvent(newEvent);
            }
        }

        private ICollection<IEventsProcessor> observers_ = new LinkedList<IEventsProcessor>();
    }
}
