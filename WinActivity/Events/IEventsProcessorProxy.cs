using System;
using System.Collections.Generic;
using System.Text;

namespace WinActivityApplication.Events
{
    public interface IEventsProcessorProxy
    {
        void addObserver(IEventsProcessor observer);

        void removeObserver(IEventsProcessor observer);

        void notifyObservers(IEvent newEvent);
    }
}
