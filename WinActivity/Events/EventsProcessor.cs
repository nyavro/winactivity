using System;
using System.Collections.Generic;
using System.Text;
using WinActivityApplication.Model;

namespace WinActivityApplication.Events
{
    public class EventsProcessor: IEventsProcessor
    {
        public EventsProcessor(IModel model)
        {
            model_ = model;
        }

        public void processEvent(IEvent newEvent)
        {
            model_.addEvent(newEvent);
        }

        private IModel model_;
    }
}
