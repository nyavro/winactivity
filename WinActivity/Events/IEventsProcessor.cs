using System;
using System.Collections.Generic;
using System.Text;

namespace WinActivityApplication.Events
{
    public interface IEventsProcessor
    {
        void processEvent(IEvent newEvent);
    }
}
