using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

namespace WinActivityApplication
{
    [Serializable]
    public abstract class AbstractEvent : IEvent
    {        
        public void setStart(DateTime time)
        {
            start = time;
        }

        public void setEnd(DateTime time)
        {            
            duration = time - start;
            isClosed = true;
        }

        public DateTime getStart()
        {
            return start;
        }

        public virtual TimeSpan getDuration()
        {
            if (!isClosed)
            {
                return DateTime.Now - start;
            }
            return duration;
        }

        public IEvent Clone()
        {

            MemoryStream ms = new MemoryStream();

            BinaryFormatter bf = new BinaryFormatter();

            bf.Serialize(ms, this);

            ms.Position = 0;

            IEvent obj = (IEvent)bf.Deserialize(ms);

            ms.Close();

            return obj;

        }

        public abstract string getTitle();        
        public abstract string getApplicationName();

        private DateTime start;
        private TimeSpan duration;
        private bool isClosed = false;
    }
}
