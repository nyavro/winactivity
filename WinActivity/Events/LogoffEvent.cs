using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;

namespace WinActivityApplication
{
    [Serializable]
    public class LogoffEvent : AbstractEvent
    {
        public LogoffEvent()
        {        
        }

        public override string getTitle()
        {
            return Properties.i18n.LogoffEventTitle;
        }        

        public override string getApplicationName()
        {
            return Properties.i18n.LogoffProcessName;
        }

    }
}
