using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using System.Runtime.Serialization;

namespace WinActivityApplication
{    
    public interface IEvent
    {
        string getTitle();
        void setStart(DateTime time);
        void setEnd(DateTime time);
        DateTime getStart();
        TimeSpan getDuration();        
        string getApplicationName();
        IEvent Clone();
    }
}
