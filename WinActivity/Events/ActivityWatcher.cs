using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using Microsoft.Win32;
using WinActivityApplication.Events;
using WinActivityApplication.Database;

namespace WinActivityApplication
{
    public class EventsGenerator
    {
        public EventsGenerator(ILifetimeProvider provider, EventsBuffer buffer, ApplicationsDBCache cache)
        {
            lifetimeProvider = provider;
            eventsBuffer = buffer;
            cache_ = cache;
        }

        public void watch()
        {
            SystemEvents.SessionSwitch += new SessionSwitchEventHandler(SystemEvents_SessionSwitch); 
            while (lifetimeProvider.isAlive())
            {                
                IEvent newEvent;
                TimeSpan inactivityTime = APIUtils.getInactivityTime();
                if (inactivityTime.CompareTo(INACTIVITY_LIMIT) > 0)
                {
                    newEvent = new InactivityEvent();
                    newEvent.setStart(DateTime.Now - inactivityTime);                    
                    eventsBuffer.addEvent(newEvent, inactivityTime);               
                }
                else
                {
                    ProcessDescriptor descr = ProcessDescriptor.getAcitveProcess(cache_);
                    newEvent = new ActivityEvent(descr.getApplicationName(), descr.getTitle());
                    newEvent.setStart(DateTime.Now);
                    eventsBuffer.addEvent(newEvent);               
                }                
                
                Thread.Sleep(1000);                
            }
        }

        void SystemEvents_SessionSwitch(object sender, SessionSwitchEventArgs e)
        {
            SessionSwitchReason r = e.Reason;
            Logger.logger.write(r.ToString());
            IEvent logoffEvent = new LogoffEvent();            
            logoffEvent.setStart(DateTime.Now);
            eventsBuffer.addEvent(logoffEvent);  
        }

        private ILifetimeProvider lifetimeProvider;
        private EventsBuffer eventsBuffer;
        private TimeSpan INACTIVITY_LIMIT = new TimeSpan(0, 0, 20);
        private ApplicationsDBCache cache_;
    }
}
