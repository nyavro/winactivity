using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using Microsoft.Win32;
using WinActivityApplication.Events;
using WinActivityApplication.Database;

namespace WinActivityApplication
{
    public class EventsGenerator
    {
        public static Thread create(ILifetimeProvider lifetimeProvider, ApplicationsDBCache cache, IEventsProcessorProxy proxy)
        {
            EventsGenerator generator = new EventsGenerator(lifetimeProvider, cache, proxy);
            Thread eventsGeneratorThread = new Thread(new ThreadStart(generator.generate));
            eventsGeneratorThread.Name = "EventsGenerator";
            return eventsGeneratorThread;        
        }        
   
        private EventsGenerator(ILifetimeProvider provider, ApplicationsDBCache cache, IEventsProcessorProxy proxy)
        {            
            lifetimeProvider = provider;            
            cache_ = cache;
            proxy_ = proxy;
        }        

        private void generate()
        {
            SystemEvents.SessionSwitch += new SessionSwitchEventHandler(SystemEvents_SessionSwitch);
            SystemEvents.PowerModeChanged += new PowerModeChangedEventHandler(SystemEvents_PowerMode);
            
            while (lifetimeProvider.isAlive())
            {
                Thread.Sleep(1000);
                IEvent newEvent;
                if (APIUtils.getInactivityTime().CompareTo(INACTIVITY_LIMIT) > 0)
                {
                    newEvent = new InactivityEvent();
                }
                else
                {
                    ProcessDescriptor descr = ProcessDescriptor.getAcitveProcess(cache_);
                    newEvent = new ActivityEvent(descr.getApplicationName(), descr.getTitle());
                }
                processEvent(newEvent);                
            }
        }        

        private void SystemEvents_SessionSwitch(object sender, SessionSwitchEventArgs e)
        {
            Console.WriteLine("Session " + e.ToString() + " " + e.Reason);
            //Console.WriteLine
            /*IEvent logoffEvent = new LogoffEvent();
            logoffEvent.setStart(DateTime.Now);
            processEvent(logoffEvent);  */
        }

        private void SystemEvents_PowerMode(object sender, PowerModeChangedEventArgs e)
        {                        
            Console.WriteLine("PowerMode " + e.ToString() + " " + e.Mode);
        }

        private void processEvent(IEvent newEvent)
        {
            if (isEventNew(newEvent))
            {
                if (currentEvent_ != null)
                {
                    currentEvent_.setEnd(DateTime.Now);
                }

                newEvent.setStart(DateTime.Now);
                currentEvent_ = newEvent;
                proxy_.notifyObservers(newEvent);
            }            
        }

        private bool isEventNew(IEvent activityEvent)
        {
            if (currentEvent_ == null || currentEvent_.getTitle() == null)
            {
                return true;
            }
            string title = activityEvent.getTitle();

            if (title == null)
            {
                return false;
            }
            return !currentEvent_.getTitle().Equals(title);
        }

        private IEvent currentEvent_;
        private ILifetimeProvider lifetimeProvider;        
        private TimeSpan INACTIVITY_LIMIT = new TimeSpan(0, 0, 20);
        private ApplicationsDBCache cache_;        
        private IEventsProcessorProxy proxy_;        
    }
}
