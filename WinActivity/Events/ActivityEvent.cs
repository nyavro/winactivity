using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;

namespace WinActivityApplication
{
    [Serializable]
    public class ActivityEvent : AbstractEvent
    {
        public ActivityEvent(string applicationName, string title, DateTime time, TimeSpan duration)
        {
            applicationName_ = applicationName;
            title_ = title;
            setStart(time);                        
            setEnd (time+duration);
        }

        public ActivityEvent(string applicationName, string title)
        {
            applicationName_ = applicationName;
            title_ = title;
        }

        public override string getTitle()
        {
            return title_;
        }        

        public override string getApplicationName()
        {
            return applicationName_;
        }

        public override string ToString()
        {
            return applicationName_ + " " + getStart() + " " + getDuration();
        }

        private string applicationName_;
        private string title_;        
    }
}
