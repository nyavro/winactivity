using System;
using System.Collections.Generic;
using System.Text;

namespace WinActivityApplication.Controls
{
    public class Scale
    {
        public Scale(TimeSpan start, TimeSpan end)
        {
            setStart(start);
            setEnd(end);
        }

        public DateTime getStart()
        {
            return zeroTime_ + startSpan_;
        }

        public DateTime getEnd()
        {
            return zeroTime_ + endSpan_;
        }

        public TimeSpan getSpan()
        {
            return endSpan_ - startSpan_;
        }

        public void setStart(TimeSpan span)
        {
            startSpan_ = span;
        }

        public void setEnd(TimeSpan span)
        {
            endSpan_ = span;
        }

        public void setZero(DateTime zero)
        {
            zeroTime_ = zero;
        }

        public int timeToPosition(DateTime time, int max)
        {
            return (int)Math.Floor(doubleSpanToWidth(time - getStart(), max));
        }

        public int spanToWidth(TimeSpan span, int max)
        {
            return (int)Math.Ceiling(doubleSpanToWidth(span, max));
        }

        private double doubleSpanToWidth(TimeSpan span, int max)
        {
            double spanRate = (double)span.Ticks / getSpan().Ticks;
            return spanRate * max;
        }

        public DateTime timeByPosition(int scalePos, int max)
        {
            float r = (float)scalePos / max;
            return getStart() + new TimeSpan((long)(getSpan().Ticks * r));
        }

        public TimeSpan getStepValue(int width)
        {
            return new TimeSpan(getSpan().Ticks / width);            
        }

        private TimeSpan startSpan_;
        private TimeSpan endSpan_;
        private DateTime zeroTime_;
    }
}
