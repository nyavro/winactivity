using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using WinActivityApplication.Model;
using WinActivityApplication.Views;
using System.IO;

namespace WinActivityApplication.Controls
{
    public partial class Timeline : UserControl
    {
        public Timeline()
        {
            InitializeComponent();
            //drawTooltip();    
           // this.Controls.Add(customToolTip_.getControl());
            
        }

        private Bitmap bitmap_;
        private Bitmap timeScale_;
        private Bitmap tooltip_;
        private const int TIMESCALE_HEIGHT = 16;        

        private void drawTimeScale(TimeSpan minInterval)
        {
            timeScale_ = new Bitmap(Width, TIMESCALE_HEIGHT);
            Graphics g = Graphics.FromImage(timeScale_);
            g.Clear(TIMESCALE_COLOR);
            g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;

            DateTime cur = scale_.getStart();
            DateTime end = scale_.getEnd();            

            Pen pen = new Pen(TIMESCALE_TEXT_COLOR);
            g.DrawLine(pen, 0, 0, Width, 0);

            while (cur < end)
            {
                int x = scale_.timeToPosition(cur, Width - 1);
                g.DrawLine(pen, x, 0, x, TIMESCALE_HEIGHT);
                g.DrawString(Utils.getShortTime(cur), TIMESCALE_FONT, new SolidBrush(TIMESCALE_TEXT_COLOR), new PointF(x + TIMESCALE_TEXT_PADDING, TIMESCALE_TEXT_PADDING));
                cur = cur + minInterval;
            }

            pen.Dispose();

            g.Dispose();
        }

        private void drawEmptyTimelineWithBorder(Graphics g, int height)
        {            
            g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
            g.Clear(TIMELINE_COLOR);
            Pen pen = new Pen(TIMELINE_BORDER_COLOR);
            g.DrawRectangle(pen, 0, 0, Width, height);
            pen.Dispose();
        }        

        private void drawTimeline(int height)
        {
            Console.WriteLine(scale_.getStepValue(Width));
            //Console.WriteLine("e");
            bitmap_ = new Bitmap(Width, height);
            Graphics g = Graphics.FromImage(bitmap_);

            drawEmptyTimelineWithBorder(g, height);

            if (model_ != null)
            {
                IEnumerator<IEvent> enumerator = model_.getEvents();
                if (model_.getActiveEvent() != null)
                {
                    process(model_.getEvents(), g);
                    //Console.WriteLine("ActiveEvent" + model_.getActiveEvent().getApplicationName());
                    //drawEvent(model_.getActiveEvent(),height - TIMELINE_BOTTOM_PADDING, g);
                }
                else
                {
                    process(model_.getEvents(), g);
                }
            }

            g.Dispose();
        }

        private void process(IEnumerator<IEvent> enumerator, Graphics g)
        {
            TimeSpan stepValue = scale_.getStepValue(Width);
            DateTime prevEnd = new DateTime(0);
            while (enumerator.MoveNext())
            {
                IEvent curEvent = enumerator.Current;

                DateTime curStart = curEvent.getStart();
                DateTime curEnd = curEvent.getStart() + curEvent.getDuration();

                if(prevEnd == new DateTime(0)) 
                {
                    drawRegion(curStart, curEnd, curEvent.getApplicationName(), g);
                }
                else 
                {
                    if ((curStart - prevEnd) < stepValue)
                    {
                        drawRegion(prevEnd, curEnd, curEvent.getApplicationName(), g);
                    }
                    else
                    {
                        drawRegion(curStart, curEnd, curEvent.getApplicationName(), g);
                    }
                }
                
                prevEnd = curEnd;
            }           
        }

        private void drawRegion(DateTime start, DateTime end, String name, Graphics g)
        {
            if (end < scale_.getStart())
            {
                return;
            }

            Rectangle rect = new Rectangle(
                scale_.timeToPosition(start, Width),
                TIMELINE_TOP_PADDING,
                scale_.spanToWidth(end - start, Width) + 1,
                Height - TIMELINE_TOP_PADDING
            );
           
            Color color = getColor(name);
            g.FillRectangle(new SolidBrush(color), rect);
            
        }             

        private void backgroundRedraw()
        {
            TimeSpan scaleSpan = scale_.getSpan();
            int minutes = (scaleSpan.Minutes + scaleSpan.Hours * 60) / intervalsCount_;
            int minutesRoundedTo10 = Math.Max((minutes / 10) * 10, 10);

            drawTimeScale(new TimeSpan(minutesRoundedTo10 / 60, minutesRoundedTo10 % 60, 0));
            drawTimeline(Height - TIMESCALE_HEIGHT);
        }

        private void paint(object sender, PaintEventArgs e)
        {
            Console.WriteLine("t");
            if (e.ClipRectangle.IsEmpty)
            {
                return;
            }

            backgroundRedraw();

            Console.WriteLine("paint " + new TimeSpan(scale_.getSpan().Ticks / Width) + " per pixel");
            Graphics displayGraphics = e.Graphics;


            displayGraphics.DrawImageUnscaled(bitmap_, 0, TIMESCALE_HEIGHT);
            displayGraphics.DrawImageUnscaled(timeScale_, 0, 0);
            displayGraphics.Dispose();
        }

        private Color getColor(String name)
        {
            Random random = new Random();
            Color color;
            if (colors.ContainsKey(name))
            {
                color = colors[name];
            }
            else
            {
                Image icon = iconsManager_.getImage(name);
                Bitmap bitmap = new Bitmap(icon);
                int r = 0;
                int g = 0;
                int b = 0;
                int a = 0;
                int count = 1;
                for(int i=0;i<bitmap.Width;i++) {
                    for(int j=0;j<bitmap.Height;j++) {
                        Color px = bitmap.GetPixel(i, j);
                        if (!isGray(px, 0))
                        {
                            a += px.A;
                            r += px.R;
                            g += px.G;
                            b += px.B;
                            count++;
                        }
                    }
                }
                int sensitivity = 1;
                color = Color.FromArgb(
                    round(a / count, sensitivity),
                    round(r / count, sensitivity),
                    round(g / count, sensitivity),
                    round(b / count, sensitivity));//Color.FromArgb(random.Next(255), random.Next(255), random.Next(255));
                colors[name] = color;
            }
            return color;
        }

        private Boolean isGray(Color color, int range)
        {
            return Math.Abs(color.R - color.G) < range && Math.Abs(color.R - color.B) < range && Math.Abs(color.G - color.B) < range;          
        }

        private int round(int v, int roundBase)
        {
            return (v / roundBase) * roundBase;
        }

        public void process(IEvent newEvent)
        {
            //Console.WriteLine("Timeline:process");
            //this.Invalidate();
            DateTime end = model_.getEnd();
            scale_.setEnd(new TimeSpan(end.Hour, end.Minute, end.Second));
            backgroundRedraw();
        }

        public void setItems(IModel model)
        {
            DateTime start = model.getStart();
            DateTime end = model.getEnd();

            scale_ = new Scale(new TimeSpan(start.Hour, 0, 0), new TimeSpan(end.Hour, end.Minute, end.Second));
            model_ = model;
            scale_.setZero(DateTimeUtil.getDayStart(model.getDateRange().start));
            this.Refresh();            
        }

        private void Timeline_MouseMove(object sender, MouseEventArgs e)
        {
            if (prevMousePos.X != e.X)
            {
                Console.WriteLine(e.X);
                drawCursor(e.X, e.Y);
                prevMousePos.X = e.X;
                prevMousePos.Y = e.Y;
            }
        }

        private void drawCursor(int scalePosition, int y)
        {          
            Console.Write("c");
            Graphics graphics = Graphics.FromHwnd(this.Handle);

            DateTime time = scale_.timeByPosition(scalePosition, Width);
            IEvent ev = model_.getEvent(time);
            Point globalLeftTop = PointToScreen(Location);

            

            if (ev != null)
            {
                if (!ev.Equals(prevEvent))
                {
                    //ttf_.Show();
                    ttf_.setIcon(iconsManager_.getImage(ev.getApplicationName()));
                    ttf_.showTooltip(this, time, ev);
                }                
            }
            else
            {
                ttf_.setIcon(null);
                ttf_.showTooltip(this, time, null);
            }

            int yPos = globalLeftTop.Y + TOOLTIP_OFFSET;
            int xPos = globalLeftTop.X + scalePosition - ttf_.Width / 2;
            ttf_.Location = new Point(xPos, yPos);
            prevEvent = ev;
            //line_.Location = new Point(globalLeftTop.X + scalePosition, globalLeftTop.Y - Height);

            Bitmap tmpBitmap_ = new Bitmap(Width, Height);
            Graphics g = Graphics.FromImage(tmpBitmap_);

            graphics.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
            
            g.DrawImageUnscaled(bitmap_, 0, TIMESCALE_HEIGHT);
            g.DrawImageUnscaled(timeScale_, 0, 0);
              
            Pen pen = new Pen(Color.Black);
            g.DrawLine(pen, scalePosition, 0, scalePosition, Height);
            pen.Dispose();
            g.Dispose();

            graphics.DrawImageUnscaled(tmpBitmap_, 0, 0);
            graphics.Dispose();         
        }

        public void setIconsCache(IconsManager manager)
        {
            iconsManager_ = manager;
        }

        public void setIntervalsCount(int count)
        {
            intervalsCount_ = count;
        }

        public Scale getScale()
        {
            return scale_;
        }

        private void Timeline_MouseLeave(object sender, EventArgs e)
        {
            prevEvent = null;
            ttf_.Hide();
        }

        private void Timeline_MouseEnter(object sender, EventArgs e)
        {
            ttf_.Show(this);
            Focus();
        }

        private IEvent prevEvent = null;
        private IModel model_;
        private Dictionary<string, Color> colors = new Dictionary<string, Color>();        
        private Point prevMousePos = new Point();
        private Color TIMESCALE_COLOR = Color.White;
        private Color TIMELINE_COLOR = Color.WhiteSmoke;
        private Color TIMELINE_BORDER_COLOR = Color.Tomato;
        private Color TIMESCALE_TEXT_COLOR = Color.BlueViolet;
        private int TIMELINE_TOP_PADDING = 1;
        private int TIMELINE_BOTTOM_PADDING = 1;
        private int TIMESCALE_TEXT_PADDING = 1;
        private int TIMESCALE_TEXT_SIZE = 7;
        private int TOOLTIP_OFFSET = 10;
        private Font TIMESCALE_FONT = new Font("Arial", 7);
        private ToolTip tooltip = new ToolTip();
        private IconsManager iconsManager_;
        private TimelineToolTip customToolTip_ = new TimelineToolTip();
        private int intervalsCount_ = 10;
        private ToolTipForm ttf_ = new ToolTipForm();
        private Scale scale_;
        private LineCursor line_ = new LineCursor();
    }
}
