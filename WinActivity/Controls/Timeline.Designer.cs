using System.Windows.Forms;
namespace WinActivityApplication.Controls
{
    partial class Timeline
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);            
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.SuspendLayout();
            // 
            // Timeline
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Name = "Timeline";
            this.Size = new System.Drawing.Size(473, 51);
            this.MouseLeave += new System.EventHandler(this.Timeline_MouseLeave);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.paint);
            this.MouseMove += new System.Windows.Forms.MouseEventHandler(this.Timeline_MouseMove);
            this.MouseEnter += new System.EventHandler(this.Timeline_MouseEnter);
            this.ResumeLayout(false);
           // this.SetStyle(ControlStyles.OptimizedDoubleBuffer, true);
        }

        #endregion
    }
}
