namespace WinActivityApplication.Controls
{
    partial class InfoPanel
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dayStartLabel = new System.Windows.Forms.Label();
            this.dayEndLabel = new System.Windows.Forms.Label();
            this.dayDurationLabel = new System.Windows.Forms.Label();
            this.dayStartValue = new System.Windows.Forms.Label();
            this.dayEndValue = new System.Windows.Forms.Label();
            this.dayDurationValue = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // dayStartLabel
            // 
            this.dayStartLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.dayStartLabel.Location = new System.Drawing.Point(15, 15);
            this.dayStartLabel.Name = "dayStartLabel";
            this.dayStartLabel.Size = new System.Drawing.Size(120, 20);
            this.dayStartLabel.TabIndex = 0;
            this.dayStartLabel.Text = "Day start";
            this.dayStartLabel.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // dayEndLabel
            // 
            this.dayEndLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.dayEndLabel.Location = new System.Drawing.Point(155, 15);
            this.dayEndLabel.Name = "dayEndLabel";
            this.dayEndLabel.Size = new System.Drawing.Size(120, 20);
            this.dayEndLabel.TabIndex = 0;
            this.dayEndLabel.Text = "Day end";
            this.dayEndLabel.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // dayDurationLabel
            // 
            this.dayDurationLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.dayDurationLabel.Location = new System.Drawing.Point(295, 15);
            this.dayDurationLabel.Name = "dayDurationLabel";
            this.dayDurationLabel.Size = new System.Drawing.Size(120, 20);
            this.dayDurationLabel.TabIndex = 0;
            this.dayDurationLabel.Text = "Duration";
            this.dayDurationLabel.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // dayStartValue
            // 
            this.dayStartValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.dayStartValue.Location = new System.Drawing.Point(15, 45);
            this.dayStartValue.Name = "dayStartValue";
            this.dayStartValue.Size = new System.Drawing.Size(120, 20);
            this.dayStartValue.TabIndex = 0;
            this.dayStartValue.Text = "00:00 AM";
            this.dayStartValue.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.dayStartValue.Click += new System.EventHandler(this.label1_Click);
            // 
            // dayEndValue
            // 
            this.dayEndValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.dayEndValue.Location = new System.Drawing.Point(155, 45);
            this.dayEndValue.Name = "dayEndValue";
            this.dayEndValue.Size = new System.Drawing.Size(120, 20);
            this.dayEndValue.TabIndex = 0;
            this.dayEndValue.Text = "00:00 PM";
            this.dayEndValue.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // dayDurationValue
            // 
            this.dayDurationValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.dayDurationValue.Location = new System.Drawing.Point(295, 45);
            this.dayDurationValue.Name = "dayDurationValue";
            this.dayDurationValue.Size = new System.Drawing.Size(120, 20);
            this.dayDurationValue.TabIndex = 0;
            this.dayDurationValue.Text = "0";
            this.dayDurationValue.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // InfoPanel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.dayDurationValue);
            this.Controls.Add(this.dayDurationLabel);
            this.Controls.Add(this.dayEndValue);
            this.Controls.Add(this.dayEndLabel);
            this.Controls.Add(this.dayStartValue);
            this.Controls.Add(this.dayStartLabel);
            this.Name = "InfoPanel";
            this.Size = new System.Drawing.Size(424, 77);
            this.ResumeLayout(false);

        }

        public void setDayStart(string value)
        {
            dayStartValue.Text = value;
        }

        public void setDayEnd(string value)
        {
            dayEndValue.Text = value;
        }

        public void setDayDuration(string value)
        {
            dayDurationValue.Text = value;
        }

        #endregion

        private System.Windows.Forms.Label dayStartLabel;
        private System.Windows.Forms.Label dayEndLabel;
        private System.Windows.Forms.Label dayDurationLabel;
        private System.Windows.Forms.Label dayStartValue;
        private System.Windows.Forms.Label dayEndValue;
        private System.Windows.Forms.Label dayDurationValue;
    }
}
