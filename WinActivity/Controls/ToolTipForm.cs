using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Threading;

namespace WinActivityApplication.Controls
{
    public partial class ToolTipForm : Form
    {
        public ToolTipForm()
        {
            InitializeComponent();
        }

        private void paint(object sender, PaintEventArgs e)
        {
            Graphics g = e.Graphics;

            if (bitmap_ != null)
            {
                g.DrawImage(bitmap_, 0, 0);
            }
            g.Dispose();
        }

        public void showTooltip(IWin32Window wnd, DateTime time, IEvent ev)
        {
            string message = WinActivityApplication.Properties.i18n.Start + " " + Utils.getShortTime(time);
            if (ev != null)
            {
                message =
                    message + "\n" +
                    WinActivityApplication.Properties.i18n.Duration + " " + Utils.getTimeSpanString(ev.getDuration()) + "\n" +                      
                    ev.getApplicationName() + "\n" + 
                    ev.getTitle();
            }
            drawTooltip(Width, Height, message);           
            Refresh();
        }

        private void drawTooltip(int width, int height, String text)
        {
            bitmap_ = new Bitmap(width, height);
            Graphics g = Graphics.FromImage(bitmap_);

            int textXoffset = 0;
            int textYoffset = 0;
            if (icon_ != null)
            {
                textYoffset = icon_.Width;
                g.DrawImage(icon_, 0, 0);
            }
            g.DrawString(text, FONT, new SolidBrush(Color.Black), new PointF(textXoffset, textYoffset));

            g.Dispose();
        }

        public void setIcon(Image icon)
        {
            icon_ = icon;
        }

        private Image icon_;
        private Color BACKGROUND_COLOR = Color.AliceBlue;
        private Font FONT = new Font("Arial", 8);
        private Size mysize = new Size(100, 100);
        private Bitmap bitmap_;

        private double pixelsPerSecond = 100;
        
    }
}