using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.Threading;

namespace WinActivityApplication.Controls
{
    public class SmoothEffect
    {
        private SmoothEffect(Form form, double value)
        {
            form_ = form;
            value_ = value;
        }

        public static void setOpacity(Form form, double value)
        {
            SmoothEffect effect = new SmoothEffect(form, value);
            Thread thread = new Thread(new ThreadStart(effect.setOpacity));
            thread.Start();
        }

        private void setOpacity()
        {
            if (form_.InvokeRequired)
            {
                operationCallback cb = new operationCallback(setOpacity);
                object[] args = new object[] {};
                form_.Invoke(cb, args);
            }
            else
            {
                doOperation();
            }
        }

        private delegate void operationCallback();

        private void doOperation()
        {
            int steps = 100;
            int delta = (int)((value_ - form_.Opacity) * steps);
            if (delta == 0)
            {
             //   return;
            }
            double direction = delta > 0 ? 1.0 / steps : -1.0 / steps;
            int range = Math.Abs(delta);
            for (int v = 0; v < range; v++)
            {
                Thread.Sleep(1);                
                form_.Opacity = form_.Opacity + direction;
            }
        }

        private Form form_;
        private double value_;
    }
}
