using System;
using System.Collections.Generic;
using System.Text;
using WinActivityApplication.Dialogs;

namespace WinActivityApplication.Security
{
    public enum RegistrationResult
    {
        firstPassed,
        passed,
        skipped,
        cancelled
    }

    public enum RegistrationState
    {
        invalid,
        trial,
        trialExpired,
        full,
        fullVersionExpired
    }

    public class RegistrationManager
    {
        public RegistrationManager(LicenseInfo license, ILicenseProvider licenseProvider)
        {
            license_ = license;
            licenseProvider_ = licenseProvider;
        }

        public RegistrationResult apply()
        {
            return RegistrationResult.passed;
            if (license_.isBroken())
            {                
                return register(false, RegistrationState.invalid);
            }

            if (license_.isTrial())
            {               
                bool isExpired = license_.isExpired();
                
                return register(!isExpired, isExpired ? RegistrationState.trialExpired : RegistrationState.trial);
            }

            if (license_.isVersionExpired())
            {                
                //User, having full license continue usage of application in trial mode
                return register(true, RegistrationState.fullVersionExpired);
            }
           
            if(isFirstRun_) {
                return RegistrationResult.firstPassed;
            }
            return RegistrationResult.passed;
        }

        private RegistrationResult register(bool allowSkip, RegistrationState state)
        {
            isFirstRun_ = true;
            previousTry_ = state;
            licenseProvider_.requestLicense(allowSkip, state);
            switch (licenseProvider_.getResult()) 
            {
                case LicenseRequestResult.provided:                
                    RegistrationManager manager = new RegistrationManager(licenseProvider_.getLicense(), licenseProvider_);
                    return manager.apply();               
                case LicenseRequestResult.cancelled:
                    return RegistrationResult.cancelled;
                case LicenseRequestResult.skipped:
                    return RegistrationResult.skipped;
            }
            return RegistrationResult.cancelled;
        }

        public LicenseInfo getLicense()
        {
            return license_;
        }

        private static LicenseInfo license_;
        private ILicenseProvider licenseProvider_;
        private static bool isFirstRun_ = false;
        private RegistrationState previousTry_; 
    }
}
