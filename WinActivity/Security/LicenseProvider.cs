using System;
using System.Collections.Generic;
using System.Text;
using WinActivityApplication.Dialogs;

namespace WinActivityApplication.Security
{
    public class LicenseProvider: ILicenseProvider
    {
        public void requestLicense(bool allowSkip, RegistrationState state)
        {
            registerForm_ = new Register(allowSkip, state);
            registerForm_.ShowDialog();            
        }

        public LicenseRequestResult getResult()
        {
            if (registerForm_ != null)
            {
                if (registerForm_.isCancelled())
                {
                    return LicenseRequestResult.cancelled;
                }
                else if (registerForm_.isContinueTrial())
                {
                    return LicenseRequestResult.skipped;
                }
                else
                {
                    return LicenseRequestResult.provided;
                }
            }
            return LicenseRequestResult.cancelled;
        }

        public LicenseInfo getLicense()
        {
            return LicenseInfo.loadFromString(registerForm_.getLicenseXML());
        }

        private Register registerForm_; 
    }
}
