using System;
using System.Collections.Generic;
using System.Text;

namespace WinActivityApplication.Security
{
    public class InvalidLicenseException : Exception
    {
        public InvalidLicenseException(string message)
            : base(message)
        {
        }
    }
}
