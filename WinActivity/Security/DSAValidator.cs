using System;
using System.Collections.Generic;
using System.Text;
using System.Security.Cryptography;

namespace WinActivityApplication.Security
{
    public class DSAValidator
    {
        public bool isValid(byte[] bytes, byte[] signature)
        {
            DSACryptoServiceProvider verifier = new DSACryptoServiceProvider();
            verifier.FromXmlString(WinActivityApplication.Properties.Resources.publicKey);

            return verifier.VerifyData(bytes, signature);
        }
    }
}
