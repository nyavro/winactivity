using System;
using System.Collections.Generic;
using System.Text;

namespace WinActivityApplication.Security
{
    public enum LicenseRequestResult
    {
        provided,
        skipped,
        cancelled
    }

    public interface ILicenseProvider
    {
        void requestLicense(bool allowSkip, RegistrationState state);
        LicenseRequestResult getResult();
        LicenseInfo getLicense();
    }
}
