using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.IO;
using WinActivity.Security;

namespace WinActivityApplication.Security
{
    public class LicenseInfo
    {
        private LicenseInfo(string signedLicense)
        {
            signedLicense_ = signedLicense;
            try
            {
                byte[] bytes = Convert.FromBase64String(signedLicense);
                init(bytes);
            }
            catch (Exception e)
            {
                isBroken_ = true;
            }
        }

        public static LicenseInfo loadFromFile(string licenseFileName)
        {
            return loadFromString(File.ReadAllText(licenseFileName));
        }

        public static LicenseInfo loadFromString(string licenseStr)
        {
            return new LicenseInfo(licenseStr);
        }

        private void init(byte[] bytes)
        {
            byte[] licenseBytes = new byte[License.getLicenseSize()];
            for (int i = 0; i < licenseBytes.Length; i++)
            {
                licenseBytes[i] = bytes[bytes.Length - License.getLicenseSize() + i];
            }
            
            byte[] signature = new byte[bytes.Length - License.getLicenseSize()];
            for (int i = 0; i < signature.Length; i++)
            {
                signature[i] = bytes[i];
            }

            DSAValidator validator = new DSAValidator();
            if (!validator.isValid(licenseBytes, signature))
            {
                isBroken_ = true;
                return;
            }
            license_ = new License(licenseBytes);
        }

        public bool isBroken()
        {
            return isBroken_;
        }

        public bool isTrial()
        {
            return license_.isTrial();
        }

        public bool isExpired()
        {
            return DateTime.Now > license_.getExpirationDate();
        }

        public bool isVersionExpired()
        {
            return (Program.getAssemblyNumber() - license_.getReleaseId()) > FREE_UPDATE_PERIOD; 
        }

        public DateTime getVersionExpirationDate()
        {
            return Program.getBirthDate().AddMonths(license_.getReleaseId() + FREE_UPDATE_PERIOD + 1);
        }

        public void saveTo(string fileName)
        {
            StreamWriter writer = new StreamWriter(fileName);
            writer.Write(signedLicense_);
            writer.Close();            
        }

        public DateTime getExpirationDate()
        {
            return license_.getExpirationDate();
        }

        public int getDaysUntilExpiration()
        {
            TimeSpan span = getExpirationDate() - DateTime.Now;
            return span.Days;
        }

        private string signedLicense_;
        private License license_;
        private bool isBroken_ = false;
        private int FREE_UPDATE_PERIOD = 24;
    }
}
