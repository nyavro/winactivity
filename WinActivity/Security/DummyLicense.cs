using System;
using System.Collections.Generic;
using System.Text;

namespace WinActivityApplication.Security
{
    public class DummyLicense
    {       
        public DummyLicense(byte[] bytes)
        {
            int y = (bytes[0]>>4) + 2010;
            int m = bytes[0] & 0xF;
            int d = bytes[1] >> 1;
            int idH = bytes[2];
            int idL = bytes[3];

            releaseId_ = bytes[4];
            id_ = (short)((idH << 8) | idL);
            isTrial_ = false;// (1 == (bytes[1] & 1));
            expirationDate_ = new DateTime(y, m, d);
        }

        public DateTime getExpirationDate()
        {
            return expirationDate_;
        }

        public byte[] toByteArray()
        {
            byte[] bytes = new byte[LICENSE_SIZE];
            int y = expirationDate_.Year - 2010;
            int m = expirationDate_.Month;
            int d = expirationDate_.Day;
            bytes[0] = (byte)((y<<4) | m);
            bytes[1] = (byte)((d<<1) | (isTrial_ ? 1 : 0));
            bytes[2] = (byte)(id_ >> 8);
            bytes[3] = (byte)(id_ & 0xFF);
            bytes[4] = releaseId_;
            return bytes;
        }    

        public bool isTrial()
        {
            return isTrial_;
        }

        public short getId()
        {
            return id_;
        }

        public byte getReleaseId()
        {
            return releaseId_;
        }

        public static int getLicenseSize()
        {
            return LICENSE_SIZE;
        }

        private static int LICENSE_SIZE = 5;
        private bool isTrial_;
        private DateTime expirationDate_;
        private short id_;
        private byte releaseId_;
    }
}
