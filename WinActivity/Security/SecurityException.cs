using System;
using System.Collections.Generic;
using System.Text;

namespace WinActivityApplication.Security
{
    public class SecurityException : Exception
    {
        public SecurityException(string message, Exception e)
            : base(message, e)
        {
        }
    }
}
