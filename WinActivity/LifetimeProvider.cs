using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.Threading;

namespace WinActivityApplication
{
    public class LifetimeProvider : ILifetimeProvider
    {        
        public void setAlive(bool val)
        {
            lock (locker)
            {
                alive = val;
            }
        }

        public bool isAlive()
        {
            lock (locker)
            {
                return alive;
            }
        }

        private bool alive = true;
        private static object locker = new object();
    }
}
