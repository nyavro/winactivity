using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using System.Runtime.InteropServices;
using System.Diagnostics;
using System.IO;
using WinActivityApplication.Database;

namespace WinActivityApplication
{
    class APIUtils
    {
        [StructLayout(LayoutKind.Sequential)]
        internal struct SHFILEINFO
        {
            public IntPtr hIcon;
            public IntPtr iIcon;
            public uint dwAttributes;
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 260)]
            public string szDisplayName;
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 80)]
            public string szTypeName;
        };

        internal struct LASTINPUTINFO
        {
            public uint cbSize;
            public uint dwTime;
        }

        #region Windows API Functions Declarations
        //This Function is used to get Active Window Title...
        [System.Runtime.InteropServices.DllImport("user32.dll", CharSet = System.Runtime.InteropServices.CharSet.Auto)]
        public static extern int GetWindowText(IntPtr hwnd, string lpString, int cch);

        //This Function is used to get Handle for Active Window...
        [System.Runtime.InteropServices.DllImport("user32.dll", CharSet = System.Runtime.InteropServices.CharSet.Auto)]
        private static extern IntPtr GetForegroundWindow();

        //This Function is used to get Active process ID...
        [System.Runtime.InteropServices.DllImport("user32.dll", CharSet = System.Runtime.InteropServices.CharSet.Auto)]
        private static extern Int32 GetWindowThreadProcessId(IntPtr hWnd, out Int32 lpdwProcessId);

        [DllImport("psapi.dll")]
        private static extern uint GetModuleBaseName(IntPtr hWnd, IntPtr hModule, StringBuilder lpFileName, int nSize);

        [DllImport("psapi.dll")]
        private static extern uint GetModuleFileNameEx(IntPtr hWnd, IntPtr hModule, StringBuilder lpFileName, int nSize);

        [DllImport("psapi.dll")]
        private static extern uint GetProcessImageFileName(Int32 hModule, StringBuilder lpFileName, int nSize);

        [DllImport("User32.dll")]
        private static extern bool GetLastInputInfo(ref LASTINPUTINFO plii);
        
        [DllImport("kernel32.dll", CharSet = CharSet.Auto, ExactSpelling = true)]
        private static extern int GetTickCount();

        #endregion

        #region User-defined Functions

        public static TimeSpan getInactivityTime()
        {
            LASTINPUTINFO info = default(LASTINPUTINFO);
            info.cbSize = (uint)Marshal.SizeOf(info);
            if (GetLastInputInfo(ref info))
            {
                long count = GetTickCount() - info.dwTime;
                DateTime lastActivity = new DateTime(info.dwTime);
                TimeSpan res = new TimeSpan(count*10000); //DateTime.Now - lastActivity;                
                return res;
            }
            return new TimeSpan(0);
        }

        public static Int32 getWindowProcessID(IntPtr hwnd)
        {            
            Int32 pid;
            GetWindowThreadProcessId(hwnd, out pid);
            return pid;
        }

        public static IntPtr getForegroundWindow()
        {            
            return GetForegroundWindow();
        }

        public static string getApplicationTitle(IntPtr hwnd)
        {
            if (hwnd.Equals(IntPtr.Zero))
            {
                return null;
            }
            string lpText = new string((char)0, 100);
            int intLength = GetWindowText(hwnd, lpText, lpText.Length);
            if ((intLength <= 0) || (intLength > lpText.Length))
            {
                return null;
            }

            string res = lpText.Substring(0, intLength);
            return res.Trim();
        }        

        class Win32
        {
            public const uint SHGFI_ICON = 0x100;
            public const uint SHGFI_LARGEICON = 0x0;    // 'Large icon
            public const uint SHGFI_SMALLICON = 0x1;    // 'Small icon

            [DllImport("shell32.dll")]
            public static extern IntPtr SHGetFileInfo(string pszPath,
                                        uint dwFileAttributes,
                                        ref SHFILEINFO psfi,
                                        uint cbSizeFileInfo,
                                        uint uFlags);
        }

        public static Icon getSmallIcon(string fName)
        {
            SHFILEINFO shinfo = new SHFILEINFO();

            IntPtr hImgSmall = Win32.SHGetFileInfo(
                fName,
                0, 
                ref shinfo,
                (uint)Marshal.SizeOf(shinfo),
                Win32.SHGFI_ICON | Win32.SHGFI_SMALLICON
            );

            if (hImgSmall == IntPtr.Zero || shinfo.hIcon == IntPtr.Zero)
            {
                return null;
            }
            else
            {                                
                return Icon.FromHandle(shinfo.hIcon);
            }           
        }        

        public static string getApplicationPath(IntPtr hwnd)
        {
            Int32 dwProcessId = getWindowProcessID(hwnd);            

            IntPtr hProcess = OpenProcess(
                ProcessAccessFlags.VMRead | ProcessAccessFlags.QueryInformation,
                false, 
                dwProcessId
            );
            
            StringBuilder path = new StringBuilder(1024);
            GetModuleFileNameEx(hProcess, IntPtr.Zero, path, 1024);

            CloseHandle(hProcess);
            return path.ToString();
        }

        public static string getApplicationProcessName(IntPtr hwnd)
        {             
            Int32 pid = getWindowProcessID(hwnd);
            Process p = Process.GetProcessById(pid);            
            return p.ProcessName;
        }

        public static string getApplicationName(string exeName)
        {
            try
            {
                FileVersionInfo info = FileVersionInfo.GetVersionInfo(exeName);
                return info.FileDescription;
            }
            catch (FileNotFoundException e)
            {
                Logger.logger.write(e);
            }
            catch (ArgumentException e)
            {
                Logger.logger.write(e);
            }
            return "";
        }

        [Flags]
        public enum ProcessAccessFlags : uint
        {
            All = 0x001F0FFF,
            Terminate = 0x00000001,
            CreateThread = 0x00000002,
            VMOperation = 0x00000008,
            VMRead = 0x00000010,
            VMWrite = 0x00000020,
            DupHandle = 0x00000040,
            SetInformation = 0x00000200,
            QueryInformation = 0x00000400,
            Synchronize = 0x00100000
        }

        [DllImport("kernel32.dll")]
        public static extern IntPtr OpenProcess(ProcessAccessFlags dwDesiredAccess, bool bInheritHandle,
           Int32 dwProcessId);

        [DllImport("kernel32.dll", SetLastError = true)]
        public static extern bool CloseHandle(IntPtr hHandle);                
    

        #endregion
    }
}
