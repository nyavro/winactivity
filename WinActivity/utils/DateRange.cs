using System;
using System.Collections.Generic;
using System.Text;

namespace WinActivityApplication
{
    public class DateRange
    {
        public DateRange(DateTime from, DateTime to)
        {
            start = from;
            end = to;
        }

        public DateRange(DateTime date)
        {
            start = date;
            end = date;
        }

        public bool contains(DateTime dateTime)
        {
            return (dateTime.CompareTo(start) >= 0 && dateTime.CompareTo(end) < 0);           
        }

        public DateTime start;
        public DateTime end;        
    }
}
