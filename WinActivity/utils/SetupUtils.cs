using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Drawing;
using WinActivityApplication.Database;

namespace WinActivityApplication.utils
{
    class SetupUtils
    {
        public static void Main2()
        {
            //saveIconsToDisk("C:\\icons");
            fixUnsplittedEvents();
        }

        public static void fixUnsplittedEvents()
        {
            DBManager.initialize(DBManager.DEFAULT_CONNECTION_STRING);

            LinkedList<IEvent> events = DBManager.getReader().getEventsByDateRange(new DateRange(new DateTime(1980, 1, 1), new DateTime(2020, 1, 1)));
            EventsSplitter splitter = new EventsSplitter(DBManager.getWriter());
            splitter.split(events.GetEnumerator());
            DBManager.close();
        }

        public static void saveIconsToDisk(string path)
        {
            //Create a new subfolder under the current active folder
            if (!Directory.Exists(path))
            {
                System.IO.Directory.CreateDirectory(path);
            }

            IDBReader manager = DBManager.getReader();
            Dictionary<string, Icon> appIcons = manager.getApplicationsIcons();
            foreach (string appName in appIcons.Keys)
            {
                
                Icon icon = appIcons[appName];

                string app = appName;
                if (appName.Equals(""))
                {
                    app = "empty";
                }

                string filePath = System.IO.Path.Combine(path, app) + ".jpg";
                icon.ToBitmap().Save(filePath);                               
            }
        }
    }
}
