using System;
using System.Collections.Generic;
using System.Text;

namespace WinActivityApplication
{
    public static class DateTimeUtil
    {
        public static LinkedList<DateTime> getToday()
        {
            return getDateRange(new DateRange(DateTime.Today));
        }

        public static LinkedList<DateTime> getYesterday()
        {
            return getDateRange(new DateRange(DateTime.Today.AddDays(-1.0))); 
        }

        public static LinkedList<DateTime> getDateRange(DateRange range)
        {
            LinkedList<DateTime> list = new LinkedList<DateTime>();

            for (DateTime i = range.start; i.CompareTo(range.end) <= 0; )
            {                
                list.AddLast(i);
                i = i.AddDays(1);
            }
            
            return list;
        }

        public static LinkedList<DateTime> getLastWeek()
        {
            return getDateRange(new DateRange(DateTime.Today.AddDays(-6.0), DateTime.Today));
        }

        public static LinkedList<DateTime> getThisMonth()
        {            
            return getDateRange(new DateRange(new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1), DateTime.Today));
        }

        public static LinkedList<DateTime> getMonth(int year, int month)
        {            
            return getDateRange(
                new DateRange(
                    new DateTime(year, month, 1), 
                    new DateTime(year, month, DateTime.DaysInMonth(year, month))
                )
            );
        }

        public static LinkedList<DateTime> getLastHalfYear()
        {
            LinkedList<DateTime> list = new LinkedList<DateTime>();
            
            for(int i=1;i<6;i++) {
                list.AddLast(DateTime.Today.AddMonths(-i));
            }

            return list;
        }

        public static DateTime getDayStart(DateTime date)
        {
            return new DateTime(date.Year, date.Month, date.Day);
        }

        public static DateTime getDayEnd(DateTime date)
        {
            return getDayStart(date).AddDays(1);
        } 

        //internal static DateTime get
    }
}
