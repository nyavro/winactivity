using System;
using System.Collections.Generic;
using System.Text;

namespace WinActivityApplication.utils
{
    public class OptionsUtils
    {
        public static int getGUIUpdatePeriod()
        {
            int res;
            if (Int32.TryParse(WinActivityApplication.Properties.Options.GuiUpdatePeriod, out res))
            {
                return res;
            }
            return 5000;
        }        
    }
}
