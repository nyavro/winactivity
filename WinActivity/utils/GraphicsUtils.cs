using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using System.Runtime.InteropServices;

namespace WinActivityApplication.utils
{
    public static class GraphicsUtils
    {
        [DllImport("user32.dll", EntryPoint = "DestroyIcon")]
        private static extern bool DestroyIcon(IntPtr hIcon);

        public static Icon convert(Image image)
        {            
            return convert(new Bitmap(image));
        }

        public static Icon convert(Bitmap bitmap)
        {
            Icon icon = null;

            IntPtr unmanagedIconHandle = bitmap.GetHicon();

            // Clone FromHandle result so we can destroy the unmanaged handle version of the icon before the converted object is passed out.
            icon = Icon.FromHandle(unmanagedIconHandle).Clone() as Icon;

            //Unfortunately, GetHicon creates an unmanaged handle which must be manually destroyed otherwise a generic error will occur in GDI+.
            DestroyIcon(unmanagedIconHandle);

            return icon;
        }
    }
}
