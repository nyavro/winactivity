using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Threading;
using System.Globalization;

namespace WinActivityApplication
{
    class Utils
    {
        public static string getTimeSpanString(TimeSpan span)
        {
            string res = span.ToString();
            int n = res.LastIndexOf('.');
            if (n != -1)
            {
                return res.Substring(0, n);
            }
            return res;
        }

        public static string getShortTimeSpan(TimeSpan span)
        {
            string res = span.ToString();
            int n = res.LastIndexOf(':');
            if (n != -1)
            {
                return res.Substring(0, n);
            }
            return res;
        }

        public static string getShortTime(DateTime time)
        {
            CultureInfo cultureInfo = Thread.CurrentThread.CurrentUICulture;            
            return time.ToString(cultureInfo.DateTimeFormat.ShortTimePattern, cultureInfo); 
        }

        public static string getShortDate(DateTime date)
        {
            CultureInfo cultureInfo = Thread.CurrentThread.CurrentUICulture;
            return date.ToString(cultureInfo.DateTimeFormat.ShortDatePattern, cultureInfo);         
        }
    }
}
