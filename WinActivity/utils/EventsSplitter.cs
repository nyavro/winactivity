using System;
using System.Collections.Generic;
using System.Text;
using WinActivityApplication.Database;
using System.Collections;

namespace WinActivityApplication.utils
{
    public class EventsSplitter
    {
        public EventsSplitter(IDBWriter writer)
        {
            writer_ = writer;
        }

        public void split(IEnumerator<IEvent> enumerator)
        {
            while (enumerator.MoveNext())
            {
                IEvent curEvent = enumerator.Current;
                DateTime start = curEvent.getStart();
                DateTime end = start + curEvent.getDuration();

                LinkedList<IEvent> events = new LinkedList<IEvent>();
                split(curEvent, events);

                if (events.Count > 1)
                {
                    writer_.remove(curEvent);
                    foreach (IEvent item in events) {
                        writer_.save(item);
                    }
                }
            }
        }

        public void split(IEvent curEvent, LinkedList<IEvent> events)
        {
            DateTime dayEnd = DateTimeUtil.getDayEnd(curEvent.getStart());
            DateTime end = curEvent.getStart() + curEvent.getDuration();
            while (end > dayEnd)
            {
                IEvent newEvent = curEvent.Clone();
                newEvent.setStart(dayEnd);
                newEvent.setEnd(end);
                curEvent.setEnd(dayEnd.AddMilliseconds(-1));
                events.AddLast(curEvent);

                curEvent = newEvent;
                dayEnd = dayEnd.AddDays(1);
            }
            events.AddLast(curEvent);
        }

        private IDBWriter writer_;
    }
}
