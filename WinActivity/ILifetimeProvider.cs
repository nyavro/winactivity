using System;
using System.Collections.Generic;
using System.Text;

namespace WinActivityApplication
{
    public interface ILifetimeProvider
    {
        bool isAlive();
        void setAlive(bool value);
    }
}
