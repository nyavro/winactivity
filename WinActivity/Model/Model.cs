using System;
using System.Collections.Generic;
using System.Text;
using WinActivityApplication.Views;

namespace WinActivityApplication.Model
{
    public class EventsModel: IModel
    {        
        public void addObserver(IView view)
        {
            observers_.AddLast(view);
        }

        public void removeObserver(IView view)
        {
            observers_.Remove(view);
        }

        private LinkedList<IView> observers_ = new LinkedList<IView>();
    }
}
