using System;
using System.Collections.Generic;
using System.Text;

namespace WinActivityApplication.Model
{
    public class Statistic
    {
        public Statistic(IEnumerator<IEvent> enumerator)
        {                       
            while (enumerator.MoveNext())
            {
                if (!statistic_.ContainsKey(enumerator.Current.getApplicationName()))
                {
                    statistic_[enumerator.Current.getApplicationName()] = new TimeSpan();
                }
                statistic_[enumerator.Current.getApplicationName()] += enumerator.Current.getDuration();
            }

            totalDuration_ = calculateTotalDuration();
        }

        public Statistic()
        {            
        }        

        public TimeSpan getDuration(string name)
        {            
            return statistic_.ContainsKey(name) ? statistic_[name] : new TimeSpan();
        }        

        public IEnumerable<string> getKeys()
        {
            return statistic_.Keys;
        }                                            

        public TimeSpan getTotalDuration()
        {
            return totalDuration_;
        }

        public void processEvent(IEvent newEvent)
        {
            string name = newEvent.getApplicationName();

            if (prevEventName_ != null)
            {
                TimeSpan span = newEvent.getDuration();
                if(prevEventName_.Equals(name)) 
                {
                    span -= prevEventDuration_;
                }

                prevEventDuration_ = newEvent.getDuration();                
                totalDuration_ += span;
                add(name, span);             
            }

            prevEventName_ = name;
        }

        private void add(string name, TimeSpan duration)
        {
            if (!statistic_.ContainsKey(name))
            {
                statistic_[name] = new TimeSpan();
            }
            
            statistic_[name] = statistic_[name] + duration;
        }

        private TimeSpan calculateTotalDuration()
        {
            TimeSpan duration = new TimeSpan();
            foreach (string name in statistic_.Keys)
            {
                duration += statistic_[name];
            }
            return duration;
        }

        private TimeSpan prevEventDuration_ = new TimeSpan();
        private string prevEventName_ = null;
        private TimeSpan totalDuration_ = new TimeSpan();
        private Dictionary<string, TimeSpan> statistic_ = new Dictionary<string, TimeSpan>();
    }
}
