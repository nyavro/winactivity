using System;
using System.Collections.Generic;
using System.Text;
using WinActivityApplication.Views;

namespace WinActivityApplication.Model
{
    public interface IModel
    {
        void addObserver(IView view);

        void removeObserver(IView view);

        void load(DateRange activeDateRange);                

        IEvent getActiveEvent();

        void update(bool fullReload);

        void addEvent(IEvent newEvent);

        IEnumerator<IEvent> getEvents();        

        DateRange getDateRange();

        IEvent getEvent(DateTime time);        

        DateTime getStart();

        DateTime getEnd();
    }
}
