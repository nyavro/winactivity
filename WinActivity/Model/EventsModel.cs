using System;
using System.Collections.Generic;
using System.Text;
using WinActivityApplication.Views;
using WinActivityApplication.Database;
using System.Threading;

namespace WinActivityApplication.Model
{
    public class EventsModel: IModel
    {
        public EventsModel(IDBReader reader)
        {
            reader_ = reader;
        }

        public void addObserver(IView view)
        {            
            lock (this)
            {
                observers_.Add(view);
            }         
        }

        public void removeObserver(IView view)
        {            
            lock (this)
            {
                observers_.Remove(view);
            }         
        }

        public void notifyObservers(bool fullReload)
        {
            ICollection<IView> observers = new LinkedList<IView>();
            lock (this)
            {
                foreach (IView view in observers_) 
                {
                    observers.Add(view);
                }
            }

            foreach (IView view in observers)
            {
                view.update(this, fullReload);
            }        
        }

        public void load(DateRange activeDateRange)
        {
            dateRange_ = activeDateRange;
            events_ = reader_.getEventsByDateRange(dateRange_);
            activeEvent_ = null;
            notifyObservers(true);
        }

        public void update(bool fullReload)
        {
            notifyObservers(fullReload);
        }        
        
        public IEvent getActiveEvent()
        {            
            lock (this)
            {
                return activeEvent_;
            }         
        }

        public void addEvent(IEvent newEvent)
        {            
            lock (this)
            {
                if (activeEvent_ != null)
                {
                    events_.AddLast(activeEvent_);
                }
                activeEvent_ = newEvent;
            }         
            notifyObservers(false); 
        }

        public IEnumerator<IEvent> getEvents()
        {
            return events_.GetEnumerator();
        }

        public DateTime getStart()
        {                        
            if (events_.Count > 0)
            {
                return events_.First.Value.getStart();
            }
            return activeEvent_.getStart();            
        }

        public DateTime getEnd()
        {
            if (activeEvent_ != null)
            {
                return activeEvent_.getStart() + activeEvent_.getDuration();
            }
            if (events_.Count > 0)
            {
                IEvent lastEvent = events_.Last.Value;
                return lastEvent.getStart() + lastEvent.getDuration();
            }
            else
            {
                return DateTime.Now;
            }
        }

        public DateRange getDateRange()
        {
            return dateRange_;
        }

        public IEvent getEvent(DateTime time)
        {
            foreach (IEvent curEvent in events_)
            {
                DateTime start = curEvent.getStart();
                if (start > time)
                {
                    return null;
                }
                DateTime end = start + curEvent.getDuration();
                if (end > time)
                {
                    return curEvent;
                }
            }
            return null;
        }

        //TODO: Remove activeEvent_
        private IEvent activeEvent_;
        private ICollection<IView> observers_ = new LinkedList<IView>();
        private LinkedList<IEvent> events_ = new LinkedList<IEvent>();
        private IDBReader reader_;
        private DateRange dateRange_;
    }
}
