using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Threading;
using WinActivityApplication.Database;
using WinActivityApplication.Dialogs;
using WinActivityApplication.Security;
using System.IO;
using WinActivityApplication.Modules;

namespace WinActivityApplication
{
    static class Program
    {
        private static int assemblyNumber = 1;
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
#if DEBUG
            string mutexName = "Global\\WinActivity_Mutex_debug";
#else
            string mutexName = "Global\\WinActivity_Mutex";
#endif
                
            using (Mutex mutex = new Mutex(false, mutexName))
            {
                if(!mutex.WaitOne(0, false))
                {
                    MessageBox.Show(WinActivityApplication.Properties.i18n.InstanceAlreadyRunning);
                    return;
                }

                
                GC.Collect();                

                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);

                
                info_ = LicenseInfo.loadFromFile(LICENSE_FILENAME);

                RegistrationManager manager = new RegistrationManager(info_, new LicenseProvider());
                switch(manager.apply()) 
                {
                    case RegistrationResult.firstPassed:
                    {
                        MessageBox.Show(Properties.i18n.RegistrationComplete);
                        setLicenseInfo(manager.getLicense());
                        break;
                    }
                    case RegistrationResult.passed:
                    {
                        setLicenseInfo(manager.getLicense());                        
                        break;
                    }
                    case RegistrationResult.skipped:
                    {
                        MessageBox.Show(string.Format(Properties.i18n.RegistrationSkipped, info_.getDaysUntilExpiration()));
                        break;
                    }
                    case RegistrationResult.cancelled:
                    {
                        return;
                    }
                }
                DBManager.initialize(DBManager.DEFAULT_CONNECTION_STRING);

                IServerApplication serverApplication = new ServerApplication();
                InvisibleForm serverForm = new InvisibleForm(serverApplication);
                
                Application.Run(serverForm);
            }            
        }

        public static LicenseInfo getLicenseInfo()
        {
            return info_;
        }

        public static void setLicenseInfo(LicenseInfo info)
        {
            info_ = info;
            info_.saveTo(LICENSE_FILENAME);
        }

        public static int getAssemblyNumber()
        {            
            return 1;
        }

        public static DateTime getBirthDate()
        {
            return new DateTime(2010, 10, 1);
        }

        private static LicenseInfo info_;
        private static string LICENSE_FILENAME = "license.xml";
    }
}