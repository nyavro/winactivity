using System;
using System.Collections.Generic;
using System.Text;
using System.Data.OleDb;
using System.Globalization;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Collections;
using WinActivityApplication.Database;
using System.Windows.Forms;
using System.Data;
using WinActivityApplication.utils;

namespace WinActivityApplication
{
    public class DBManager: IDBWriter, IDBReader
    {
        private DBManager(string connectionString)
        {                        
            //backend_ = new OleDbBackend(string.Format("Provider=Microsoft.Jet.OLEDB.4.0;Data Source={0}\\events.mdb;User Id=admin;Password=;", Path.GetDirectoryName(Application.ExecutablePath)));            
            try
            {
                Console.WriteLine("Creation: ");
                backend_ = new SQLLiteBackend(connectionString);
                initializeDB();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Creation: " + ex.Message);
                Logger.logger.write(ex);
            }  
        }

        private void initializeDB()
        {
            executeNonQuery(QUERY_CREATE_TABLE_APPLICATIONS);
            executeNonQuery(QUERY_CREATE_TABLE_EVENTS);
        }
        
        public static IDBReader getReader()
        {
            return instance;
        }

        public static IDBWriter getWriter()
        {
            return instance;
        }

        public LinkedList<string> getApplications()
        {
            LinkedList<string> res = new LinkedList<string>();

            string query = QUERY_GET_APPLICATION_IDS;

            lock (this)
            {                
                try
                {
                    IDbConnection connection = backend_.getConnection();
                    IDbCommand command = backend_.getCommand(query);
                    command.Connection = connection;
                    IDataReader reader = command.ExecuteReader();

                    while (reader.Read())
                    {
                        res.AddLast(reader.GetString(0));
                    }

                    backend_.finishConnection(connection);
                }
                catch (Exception ex)
                {                        
                    Logger.logger.write(ex);
                }                
            }
            return res;
        }

        public LinkedList<DateTime> getDatesWithEvents(DateTime start, DateTime end)
        {
            LinkedList<DateTime> res = new LinkedList<DateTime>();

            string query = string.Format(
                QUERY_GET_DAYS_WITH_EVENTS,
                start.ToString("yyyy-MM-dd"),
                end.ToString("yyyy-MM-dd")
            );

            lock (this)
            {                
                try
                {
                    IDbConnection connection = backend_.getConnection();
                    IDbCommand command = backend_.getCommand(query);                    
                    IDataReader reader = command.ExecuteReader();
                    //---------------------------------------------------------
                    while (reader.Read())
                    {
                        int day = reader.GetInt16(0);
                        int month = reader.GetInt16(1);
                        int year = reader.GetInt16(2);

                        res.AddLast(new DateTime(year, month, day));
                    }
                    //==========================================================
                    backend_.finishConnection(connection);
                }
                catch (Exception ex)
                {
                    Logger.logger.write(ex);
                }                
            }

            return res;
        }

        public Dictionary<string, Icon> getApplicationsIcons()
        {
            Dictionary<string, Icon> res = new Dictionary<string, Icon>();
            lock (this)
            {                
                try
                {
                    IDbConnection connection = backend_.getConnection();
                    IDbCommand command = backend_.getCommand(QUERY_GET_APPLICATIONS_ICONS);
                    IDataReader reader = command.ExecuteReader();

                    //---------------------------------------------------------
                    while (reader.Read())
                    {
                        string applicationName = reader.GetString(0);
                        Type type = reader.GetFieldType(1);

                        byte[] buffer = new byte[16384];    //  max : 256x256
                        long size = reader.GetBytes(1, 0, buffer, 0, 16384);

                        Stream stream = new MemoryStream(buffer);
                        Bitmap bmp = new Bitmap(stream);

                        res[applicationName] = GraphicsUtils.convert(bmp);
                    }
                    //==========================================================
                    backend_.finishConnection(connection);
                }
                catch (Exception ex)
                {
                    Logger.logger.write(ex);
                }                
            }
            return res;
        }

        public LinkedList<IEvent> getEventsByDateRange(DateRange range)
        {
            LinkedList<IEvent> res = new LinkedList<IEvent>();

            string getEventsQuery = string.Format(
                QUERY_GET_DATE_RANGE_EVENTS,
                range.start.ToString("yyyy-MM-dd"),
                range.end.ToString("yyyy-MM-dd")
            );
            

            lock (this)
            {                
                try
                {
                    IDbConnection connection = backend_.getConnection();
                    IDbCommand command = backend_.getCommand(getEventsQuery);                    
                    IDataReader reader = command.ExecuteReader();

                    //--------------------------------------------------------------
                    while (reader.Read())
                    {
                        string applicationTitle = reader.GetString(0);
                        string applicationName = reader.GetString(1);
                        DateTime time = reader.GetDateTime(2);
                        long dur = reader.GetInt32(3);
                        
                        res.AddLast(
                            new ActivityEvent(applicationName, applicationTitle, time, fromDBFormat(dur))
                        );
                    }
                    //==============================================================
                    backend_.finishConnection(connection);
                }
                catch (Exception ex)
                {
                    Logger.logger.write(ex);
                }               
            }
            return res;
        }

        public void addApplication(string applicationName, Icon icon)
        {
            string query = string.Format(QUERY_ADD_APPLICATION, applicationName);
            lock (this)
            {                                
                try
                {
                    IDbConnection connection = backend_.getConnection();
                    IDbCommand command = backend_.getCommand(query);                    

                    byte[] buffer = new byte[16384];    //  max : 256x256
                    Stream stream = new MemoryStream(buffer);
                    icon.ToBitmap().Save(stream, ImageFormat.Png);

                    IDbDataParameter param = backend_.createBinaryParameter(buffer);

                    command.Parameters.Add(param);                    
                    command.ExecuteNonQuery();
                    backend_.finishConnection(connection);                
                }
                catch (Exception ex)
                {
                    Logger.logger.write(ex);
                }                
            }
        }

        public void save(IEvent newEvent)
        {   
            string strInsert = string.Format(
                QUERY_SAVE_EVENT,
                newEvent.getStart().ToString("yyyy-MM-dd HH:mm:ss"),
                newEvent.getTitle(),
                newEvent.getApplicationName(),
                toDBFormat(newEvent.getDuration())
            );
            Console.WriteLine(strInsert);
            executeNonQuery(strInsert);
        }

        private void executeNonQuery(string query)
        {
            lock (this)
            {
                try
                {
                    IDbConnection connection = backend_.getConnection();
                    IDbCommand command = backend_.getCommand(query);                    
                    command.ExecuteNonQuery();
                    //------------------------------------------------------------------                    
                    //==================================================================
                    backend_.finishConnection(connection);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    Logger.logger.write(ex);
                }
            }
        }             

        private TimeSpan fromDBFormat(long dbFormat)
        {
            return new TimeSpan(dbFormat * TimeSpan.TicksPerSecond);
        }

        private long toDBFormat(TimeSpan span)
        {
            return span.Ticks / TimeSpan.TicksPerSecond;
        }

        private static DBManager init()
        {
            return new DBManager(DEFAULT_CONNECTION_STRING);
        }

        public const string DEFAULT_CONNECTION_STRING = "Data Source=ev.db;Version=3;New=False;Compress=True;";
        /*
        public static void init(string connectionString)
        {
            if (instance != null && instance.backend_!=null)
            {
                instance.backend_.close();
            }
            instance = new DBManager(connectionString);
        }
        */
        public static void close()
        {
            instance.backend_.close();
            instance = null;
        }

        public static void initialize(string connectionString)
        {
            //if (instance == null)
            {
                instance = new DBManager(connectionString);
            }
        }

        public void remove(IEvent remove)
        {
            string query = string.Format(
                QUERY_REMOVE_EVENT,
                remove.getStart().ToString("yyyy-MM-dd HH:mm:ss"),
                remove.getTitle(),
                remove.getApplicationName()
            );            
            executeNonQuery(query);
        }

        private static DBManager instance = null;
/*
        private const string QUERY_ADD_APPLICATION = "INSERT INTO applications (applicationName, icon) VALUES ('{0}', ?)";
        private const string QUERY_GET_APPLICATION_IDS = "SELECT applicationName FROM applications";
        private const string QUERY_GET_APPLICATIONS_ICONS = "SELECT applicationName, icon FROM applications";
        //private const string QUERY_LAST_INSERTED_ID = "SELECT @@IDENTITY";
        private const string QUERY_SAVE_EVENT = "INSERT INTO activityEvents (eventTime, applicationTitle, applicationName, duration) VALUES ('{0}', '{1}', '{2}', {3})";
        private const string QUERY_GET_DATE_RANGE_EVENTS = "SELECT applicationTitle, applicationName, eventTime, duration FROM activityEvents WHERE eventTime>=#{0}# AND eventTime<#{1}# ORDER BY eventTime";
        private const string QUERY_GET_DATE_RANGE_STATISTICS = "SELECT applicationName, totalDuration FROM (SELECT applicationName, sum(duration) as totalDuration FROM activityEvents WHERE eventTime>=#{0}# AND eventTime<#{1}# GROUP BY applicationName) WHERE totalDuration>0";
        private const string QUERY_GET_DAYS_WITH_EVENTS = "SELECT distinct DAY(eventTime), MONTH(eventTime), YEAR(eventTime) FROM activityEvents WHERE eventTime>=#{0}# AND eventTime<#{1}#";
*/
        private const string QUERY_ADD_APPLICATION = "INSERT INTO applications (applicationName, icon) VALUES ('{0}', @icon)";
        private const string QUERY_GET_APPLICATION_IDS = "SELECT applicationName FROM applications";
        private const string QUERY_GET_APPLICATIONS_ICONS = "SELECT applicationName, icon FROM applications";
        private const string QUERY_SAVE_EVENT = "INSERT INTO activityEvents (eventTime, applicationTitle, applicationName, duration) VALUES ('{0}', '{1}', '{2}', {3})";
        private const string QUERY_GET_DATE_RANGE_EVENTS = "SELECT applicationTitle, applicationName, eventTime, duration FROM activityEvents WHERE datetime(eventTime)>=date('{0}') AND datetime(eventTime)<date('{1}') ORDER BY date(eventTime)";
        private const string QUERY_GET_DAYS_WITH_EVENTS = "SELECT distinct STRFTIME('%d',eventTime), STRFTIME('%m',eventTime), STRFTIME('%Y',eventTime) FROM activityEvents WHERE DATETIME(eventTime)>=DATE('{0}') AND DATETIME(eventTime)<DATE('{1}')";
        private const string QUERY_REMOVE_EVENT = "DELETE FROM activityEvents WHERE eventTime='{0}' AND applicationTitle='{1}' AND applicationName='{2}'";
        
        private const string QUERY_CREATE_TABLE_APPLICATIONS = "CREATE TABLE IF NOT EXISTS applications(applicationName TEXT NOT NULL, icon BLOB NOT NULL)";
        private const string QUERY_CREATE_TABLE_EVENTS       = "CREATE TABLE IF NOT EXISTS activityEvents(eventTime TEXT NOT NULL, applicationTitle TEXT, applicationName TEXT NOT NULL, duration INTEGER)"; 
        
        private IBackend backend_;
    }
}
