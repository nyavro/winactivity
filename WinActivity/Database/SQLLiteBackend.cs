using System;
using System.Collections.Generic;
using System.Text;
using Finisar.SQLite;
using System.Data;

namespace WinActivityApplication.Database
{
    public class SQLLiteBackend : IBackend
    {
        public SQLLiteBackend(string connectionString)
        {            
            connection_ = new SQLiteConnection(connectionString);
            connection_.Open();
        }

        public IDbDataParameter createBinaryParameter(byte[] buffer)
        {
            SQLiteParameter param = new SQLiteParameter("@icon", DbType.Binary);            
            param.Value = buffer;            
            return param;
        }

        public IDbConnection getConnection()
        {
            return connection_;
        }

        public IDbCommand getCommand(String query)
        {
            SQLiteCommand command = connection_.CreateCommand();
            command.CommandText = query;            
            return command;
        }

        public void finishConnection(IDbConnection connection)
        {
        }

        public void close()
        {
            connection_.Close();
            connection_.Dispose();
        }

        private SQLiteConnection connection_;        
    }
}
