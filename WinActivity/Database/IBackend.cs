using System;
using System.Collections.Generic;
using System.Text;
using System.Data;

namespace WinActivityApplication.Database
{
    public interface IBackend
    {
        IDbDataParameter createBinaryParameter(byte[] buffer);

        IDbConnection getConnection();

        IDbCommand getCommand(String query);

        void finishConnection(IDbConnection connection);

        void close();
    }
}
