using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;

namespace WinActivityApplication.Database
{
    public interface IDBWriter
    {
        void save(IEvent newEvent);
        void addApplication(string applicationName, Icon icon);
        void remove(IEvent newEvent);
    }
}
