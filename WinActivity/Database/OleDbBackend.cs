using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.OleDb;

namespace WinActivityApplication.Database
{
    public class OleDbBackend : IBackend
    {
        public OleDbBackend(string connectionString)
        {
            connectionString_ = connectionString; 
        }

        public IDbDataParameter createBinaryParameter(byte[] buffer)
        {
            IDbDataParameter param = new OleDbParameter("icon", OleDbType.Binary);
            param.Value = buffer;
            return param;
        }

        public IDbConnection getConnection()
        {
            IDbConnection connection = new OleDbConnection(connectionString_);
            connection.Open();
            return connection;
        }

        public IDbCommand getCommand(String query)
        {
            return new OleDbCommand(query);
        }

        public void finishConnection(IDbConnection connection)
        {
            connection.Close();
        }

        public string getTablesQuery()
        {
            return QUERY_GET_TABLES;
        }

        public void close()
        {
        }

        private const string QUERY_GET_TABLES = "SELECT Name FROM MSysObjects WHERE (Left([Name],1)<>\"~\") AND (Left([Name],4) <> \"MSys\") AND ([Type] In (1, 4, 6)) ORDER BY Name";
        private string connectionString_;
    }
}
