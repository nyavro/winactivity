using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;

namespace WinActivityApplication.Database
{
    public interface IDBReader
    {
        LinkedList<IEvent> getEventsByDateRange(DateRange range);
        Dictionary<string, Icon> getApplicationsIcons();
        LinkedList<DateTime> getDatesWithEvents(DateTime start, DateTime end);
    }
}
