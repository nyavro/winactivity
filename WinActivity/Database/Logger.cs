using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Windows.Forms;

namespace WinActivityApplication.Database
{
    public class Logger
    {
        private Logger()
        {            
        }

        public static Logger logger        
        {
            get
            {
                return logger_;
            }
        }

        public void write(string message)
        {
            System.IO.StreamWriter sw = System.IO.File.AppendText(logFileName);
            try
            {
                string logLine = System.String.Format("{0:G}: {1}.", System.DateTime.Now, message);
                sw.WriteLine(logLine);
            }
            finally
            {
                sw.Close();
            }
        }

        public void write(Exception e)
        {
            write(e.Message);
            write(e.StackTrace);
        }

        private static Logger logger_ = new Logger();
        private string logFileName = Path.GetDirectoryName(Application.ExecutablePath) + "\\events.err.log";
    }
}
