using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;

namespace WinActivityApplication
{
    public class ApplicationsDBCache
    {
        public ApplicationsDBCache()
        {            
        }

        public void saveIcon(string applicationName, Icon icon)
        {
            if (icon == null || applicationName == null || applicationName.Equals(""))
            {
                return;
            }            

            if (!dict.ContainsKey(applicationName))
            {
                DBManager.getWriter().addApplication(applicationName, icon);
                dict[applicationName] = icon;
            }            
        }

        public Icon getIcon(string applicationName)
        {
            if (applicationName == null || applicationName.Equals(""))
            {
                return null;
            }
            if (dict.ContainsKey(applicationName))
            {
                return dict[applicationName];
            }

            return null;
        }


        Dictionary<string, Icon> dict = DBManager.getReader().getApplicationsIcons();
    }
}
