using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using WinActivityApplication.utils;
using WinActivityApplication.Model;
using WinActivityApplication.Controller;

namespace WinActivityApplication
{
    public class GuiDynamicUpdater
    {
        public static Thread create(ILifetimeProvider provider, IController controller)
        {
            GuiDynamicUpdater updater = new GuiDynamicUpdater(provider, controller);
            Thread guiDynamicUpdaterThread = new Thread(new ThreadStart(updater.start));
            guiDynamicUpdaterThread.Name = "GUI dynamic updater";
            return guiDynamicUpdaterThread;
        }

        private GuiDynamicUpdater(ILifetimeProvider provider, IController controller)
        {
            lifetimeProvider_ = provider;
            controller_ = controller;
        }

        private void start()
        {
            while (lifetimeProvider_.isAlive())
            {
                Thread.Sleep(OptionsUtils.getGUIUpdatePeriod());
                controller_.update();
            }            
        }

        private IController controller_;
        private ILifetimeProvider lifetimeProvider_;
    }
}
