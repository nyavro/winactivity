using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.Drawing;

namespace WinActivityApplication.Views
{
    public class CustomTooltip
    {
        public CustomTooltip()
        {
            toolTip_ = new ToolTip();
            toolTip_.OwnerDraw = true;            
            toolTip_.Draw += new DrawToolTipEventHandler(draw);
            //toolTip_.UseAnimation = true;
            toolTip_.Popup += new PopupEventHandler(popup);
            //toolTip_.AutoS
        }

        public void setIcon(Image icon)
        {
            icon_ = icon;
        }

        public void showTooltip(IWin32Window wnd, DateTime time, IEvent ev, int x, int y)
        {
            //DateTime time = getTimeByScalePosition(scalePosition);
            //IEvent ev = model_.getEvent(time);

            string message = Utils.getShortTime(time);
            if (ev != null)
            {
                message = message + "\n" + ev.getApplicationName() + "\n" + ev.getTitle();
                //tooltip.ToolTipIcon = (cache_.getIcon(ev.getApplicationName()));
            }
            drawTooltip(100, 100, message);
            toolTip_.Show(message, wnd, x, y);
        }

        private void popup(object sender, PopupEventArgs e)
        {
            Console.Write("p");
            e.ToolTipSize = mysize;

        }

        private void draw(Object sender, DrawToolTipEventArgs e)
        {                       
            Graphics g = e.Graphics;
            
            //g.Clear(BACKGROUND_COLOR);
            e.DrawBackground();
            e.DrawBorder();

            g.DrawImage(bitmap_, 0, 0);

            g.Dispose();
        }

        private void drawTooltip(int width, int height, String text)
        {
            bitmap_ = new Bitmap(width, height);
            Graphics g = Graphics.FromImage(bitmap_);

            int textXoffset = 0;
            int textYoffset = 0;
            if (icon_ != null)
            {
                textYoffset = icon_.Width;
                g.DrawImage(icon_, 0, 0);
            }
            g.DrawString(text, FONT, new SolidBrush(Color.Black), new PointF(textXoffset, textYoffset));

            g.Dispose();
        }

        private ToolTip toolTip_;
        private Image icon_;
        private Color BACKGROUND_COLOR = Color.AliceBlue;
        private Font FONT = new Font("Arial", 8);
        private Size mysize = new Size(100, 100);
        private Bitmap bitmap_;
    }
}
