using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.Globalization;
using WinActivityApplication.Views;
using WinActivityApplication.Model;

namespace WinActivityApplication.Views
{    
    public class ActivityLogView : IView
    {
        public ActivityLogView(ListView listView, IconsManager manager)
        {
            listView_ = listView;
            setIconsManager(manager);
        }

        public void update(IModel model, bool fullReload)
        {
            if (listView_.InvokeRequired)
            {
                updateCallback cb = new updateCallback(update);
                object[] args = new object[] { model, fullReload };
                listView_.Invoke(cb, args);
            }
            else
            {
                doUpdate(model, fullReload);
            }
        }

        protected void doUpdate(IModel model, bool fullReload)
        {
            if (fullReload)
            {
                setItems(model);
            }
            else
            {
                if (model.getActiveEvent() != null)
                {
                    process(model.getActiveEvent());                    
                }
            }            
        }

        private void process(IEvent newEvent)
        {
            Console.WriteLine(newEvent.getApplicationName());
            
            if (updatableItem_ != null)
            {
                if(updatableItem_.SubItems[0].Text.Equals(newEvent.getTitle()))
                {                    
                    updatableItem_.SubItems[3].Text = Utils.getTimeSpanString(newEvent.getDuration());                    
                    return;
                }
                //Items[0].SubItems[3].Text = Utils.getTimeSpanString(newEvent.getDuration());
            }            
            
            updatableItem_ = createItem(newEvent);
            listView_.BeginUpdate();
            listView_.Items.Insert(0, updatableItem_);
            listView_.EndUpdate();            
        }

        public void setItems(IModel model)
        {
            listView_.Items.Clear();
            listView_.BeginUpdate();
            IEnumerator<IEvent> enumerator = model.getEvents();
            while(enumerator.MoveNext()) 
            {
                listView_.Items.Insert(0, createItem(enumerator.Current));
            }
            if (model.getActiveEvent() != null)
            {
                updatableItem_ = createItem(model.getActiveEvent());
                listView_.Items.Insert(0, updatableItem_);
            }
            listView_.EndUpdate();
        }              

        private ListViewItem createItem(IEvent newEvent)
        {
            int imageIndex = manager_.getImageIndex(newEvent.getApplicationName());
            ListViewItem listViewItem = new ListViewItem(newEvent.getTitle(), imageIndex);
            listViewItem.SubItems.Add(new ListViewItem.ListViewSubItem(listViewItem, newEvent.getApplicationName()));
            listViewItem.SubItems.Add(new ListViewItem.ListViewSubItem(listViewItem, newEvent.getStart().ToString("T", CultureInfo.CurrentCulture)));
            listViewItem.SubItems.Add(new ListViewItem.ListViewSubItem(listViewItem, Utils.getTimeSpanString(newEvent.getDuration())));
            return listViewItem;
        }        

        private void setIconsManager(IconsManager manager)
        {
            manager_ = manager;
            listView_.SmallImageList = manager.getImageList();
            listView_.LargeImageList = manager.getImageList();
        }

        private delegate void updateCallback(IModel model, bool fullReload);
        private delegate void setDateRangeCallback(DateRange activeDateRange);

        protected IconsManager manager_;
        private ListViewItem updatableItem_;
        private ListView listView_;
    }
}
