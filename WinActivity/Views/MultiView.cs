using System;
using System.Collections.Generic;
using System.Text;
using WinActivityApplication.Model;

namespace WinActivityApplication.Views
{
    public class MultiView : IView
    {
        public void add(IView view)
        {
            childViews_.Add(view);
        }

        public void update(IModel eventsModel, bool fullReload)
        {
            foreach (IView view in childViews_)
            {
                view.update(eventsModel, fullReload);
            }
        }

        ICollection<IView> childViews_ = new LinkedList<IView>();
    }
}
