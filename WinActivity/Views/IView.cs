using System;
using System.Collections.Generic;
using System.Text;
using WinActivityApplication.Model;

namespace WinActivityApplication.Views
{
    public interface IView
    {        
        void update(IModel eventsModel, bool fullReload);
    }
}
