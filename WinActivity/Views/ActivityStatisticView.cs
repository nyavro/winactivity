using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using WinActivityApplication.Views;
using System.Drawing;
using WinActivityApplication.Model;

namespace WinActivityApplication.Views
{
    public enum SortMode {ascending, descending}

    public class ActivityStatisticView : IView
    {
        public ActivityStatisticView(ListView listView, IconsManager manager)
        {
            listView_ = listView;
            setIconsManager(manager);
        }

        public void update(IModel model, bool fullReload)
        {
            if (listView_.InvokeRequired)
            {
                updateCallback cb = new updateCallback(update);
                listView_.Invoke(cb, new object[] { model, fullReload });
            }
            else
            {
                doUpdate(model, fullReload);
            }
        }

        protected void doUpdate(IModel model, bool fullReload)
        {
            try
            {
                if (fullReload)
                {
                    statistic_ = new Statistic(model.getEvents());
                    setItems();
                }
                else
                {
                    if (model.getActiveEvent() != null)
                    {                                               
                        statistic_.processEvent(model.getActiveEvent());
                        setItems();
                    }
                }               
            }
            catch (NullReferenceException e)
            {
                string s = e.StackTrace;
            }
        }

        private void updateItem(string applicationName, TimeSpan duration)
        {
            TimeSpan totalDuration = statistic_.getTotalDuration() + duration;
            foreach (ListViewItem item in listView_.Items)
            {                
                TimeSpan span = statistic_.getDuration(item.Text);
                if (item.Text.Equals(applicationName))
                {                    
                    span = span + duration;
                    item.SubItems[1].Text = Utils.getTimeSpanString(span);
                    item.SubItems[1].Tag = span;
                }
                item.SubItems[2].Text = getDurationPercent(span, totalDuration);
                item.SubItems[2].Tag = getPercentDouble(span, totalDuration);
            }
        }

        private void setItems()
        {
            listView_.Items.Clear();
            foreach (string applicationName in statistic_.getKeys())
            {
                createItem(applicationName);                
            }            
        }

        /*private void createTotalItem()
        {
            ListViewItem listViewItem = new ListViewItem(WinActivityApplication.Properties.i18n.TotalLineText);
            listViewItem.SubItems.Add(new ListViewItem.ListViewSubItem(listViewItem, Utils.getTimeSpanString(statistic_.getTotalDuration())));
            listViewItem.SubItems.Add(new ListViewItem.ListViewSubItem(listViewItem, getDurationPercent(statistic_.getTotalDuration(), statistic_.getTotalDuration())));
            listViewItem.BackColor = Color.LightBlue;
            listViewItem.Tag = TOTAL_TAG;
            Items.Add(listViewItem);            
        }*/

        private void createItem(string applicationName)
        {
            int imageIndex = manager_.getImageIndex(applicationName);
            ListViewItem listViewItem = new ListViewItem(applicationName, imageIndex);
            listViewItem.SubItems.Add(createDurationSubItem(applicationName, listViewItem));
            listViewItem.SubItems.Add(createPercentSubItem(applicationName, listViewItem));
            listView_.Items.Insert(0, listViewItem);            
        }

        private ListViewItem.ListViewSubItem createPercentSubItem(string name, ListViewItem listViewItem)
        {
            TimeSpan v = statistic_.getDuration(name);
            TimeSpan v2 = statistic_.getTotalDuration();

            ListViewItem.ListViewSubItem item = new ListViewItem.ListViewSubItem(listViewItem, getDurationPercent(v, v2));
            item.Tag = getPercentDouble(v, v2);
            return item;
        }

        private ListViewItem.ListViewSubItem createDurationSubItem(string applicationName, ListViewItem listViewItem)
        {
            ListViewItem.ListViewSubItem item = new ListViewItem.ListViewSubItem(listViewItem, getDurationString(applicationName));
            item.Tag = statistic_.getDuration(applicationName);
            return item;
        }

        private string getDurationString(string applicationName)
        {
            return Utils.getTimeSpanString(statistic_.getDuration(applicationName));
        }

        Double getPercentDouble(TimeSpan span, TimeSpan totalSpan)
        {
            return totalSpan.Ticks == 0 ? 100.0d : (span.Ticks * 100.0f) / totalSpan.Ticks;
        }

        private string getDurationPercent(TimeSpan span, TimeSpan totalSpan)
        {                        
            return string.Format("{0:0.00}%", getPercentDouble(span, totalSpan));
        }                               

        private delegate void updateCallback(IModel model, bool fullReload);
        private delegate void setDateRangeCallback(DateRange activeDateRange);
        
        
        private void setIconsManager(IconsManager manager)
        {
            manager_ = manager;
            listView_.SmallImageList = manager.getImageList();
            listView_.LargeImageList = manager.getImageList();
        }

        public void sort(int column)
        {
            if (column == sortColumn_)
            {
                sortMode_ = sortMode_==SortOrder.Ascending ? SortOrder.Descending : SortOrder.Ascending;
            }
            else
            {
                sortColumn_ = column;
                sortMode_ = SortOrder.Ascending;
            }
            applySort();
        }

        private void applySort()
        {
            ListViewSorter sorter = new ListViewSorter(sortColumn_, sortMode_);
            listView_.ListViewItemSorter = sorter;
            listView_.Sort();
        }

        public class ListViewSorter : System.Collections.IComparer
        {
            public ListViewSorter(int column, SortOrder mode)
            {
                column_ = column;
                mode_ = mode;
            }

            public int Compare(object o1, object o2)
            {
                if (!(o1 is ListViewItem) || !(o2 is ListViewItem))
                    return 0;

                ListViewItem lvi1 = (ListViewItem)o2;
                ListViewItem lvi2 = (ListViewItem)o1;
                                
                object obj1 = lvi1.SubItems[column_].Tag;
                object obj2 = lvi2.SubItems[column_].Tag;

                IComparable cmp1 = lvi1.SubItems[column_].Text;
                IComparable cmp2 = lvi2.SubItems[column_].Text;

                if (obj1 != null && obj2 != null &&
                    obj1 is IComparable && obj2 is IComparable)
                {
                    cmp1 = (IComparable)obj1;
                    cmp2 = (IComparable)obj2;
                }
            
                return (mode_ == SortOrder.Ascending) ?
                    cmp1.CompareTo(cmp2) :
                    cmp2.CompareTo(cmp1);
            }

            private int column_;
            private SortOrder mode_;
        }   
        
        protected IconsManager manager_;  
               
        private Statistic statistic_ = new Statistic();     
        private ListView listView_;
        private int sortColumn_ = 0;
        private SortOrder sortMode_ = SortOrder.Ascending;
    }
}
