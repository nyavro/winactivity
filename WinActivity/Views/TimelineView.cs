using System;
using System.Collections.Generic;
using System.Text;
using WinActivityApplication.Controls;
using WinActivityApplication.Model;

namespace WinActivityApplication.Views
{
    public class TimelineView : IView
    {
        public TimelineView(Timeline timeline, IconsManager cache)
        {
            timeline_ = timeline;                        
            timeline_.setIconsCache(cache);
            timeline_.setIntervalsCount(10);
        }

        public void update(IModel model, bool fullReload)
        {
            if (timeline_.InvokeRequired)
            {
                updateCallback cb = new updateCallback(update);
                object[] args = new object[] { model, fullReload };
                timeline_.Invoke(cb, args);
            }
            else
            {
                doUpdate(model, fullReload);
            }
        }

        protected void doUpdate(IModel model, bool fullReload)
        {            
            if (fullReload)
            {                
                timeline_.setItems(model);
            }
            else
            {
                if (model.getActiveEvent() != null)
                {                    
                    timeline_.process(model.getActiveEvent());
                }
            }
        }
        
        private delegate void updateCallback(IModel model, bool fullReload);
        private Timeline timeline_;
    }
}
