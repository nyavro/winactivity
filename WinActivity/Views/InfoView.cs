using System;
using System.Collections.Generic;
using System.Text;
using WinActivityApplication.Controls;
using WinActivityApplication.Model;

namespace WinActivityApplication.Views
{
    public class InfoView : IView
    {
        public InfoView(InfoPanel panel)
        {
            panel_ = panel;
        }

        public void update(IModel model, bool fullReload)
        {
            if (panel_.InvokeRequired)
            {
                updateCallback cb = new updateCallback(update);
                object[] args = new object[] { model, fullReload };
                panel_.Invoke(cb, args);
            }
            else
            {
                doUpdate(model, fullReload);
            }
        }

        private void doUpdate(IModel model, bool fullReload)
        {
            if (fullReload)
            {                
                DateTime start = model.getStart();                                
                DateTime end = model.getEnd();

                start_ = start;

                panel_.setDayStart(Utils.getShortTime(start));
                panel_.setDayEnd(Utils.getShortTime(end));
                TimeSpan duration = end - start;
                panel_.setDayDuration(Utils.getShortTimeSpan(duration));
                
            }
            else
            {
                if (model.getActiveEvent() != null)
                {
                    DateTime end = model.getEnd();
                    panel_.setDayEnd(Utils.getShortTime(end));
                    TimeSpan duration = end - start_;
                    panel_.setDayDuration(Utils.getShortTimeSpan(duration));
                }
            }
        }

        private delegate void updateCallback(IModel model, bool fullReload);
        private InfoPanel panel_;
        private DateTime start_ = DateTime.Now;  
    }
}
