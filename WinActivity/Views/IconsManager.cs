using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.Drawing;

namespace WinActivityApplication
{
    public class IconsManager
    {
        public ImageList getImageList()
        {
            return imageList_;
        }

        public int getImageIndex(string applicationName)
        {
            if(imagesCache_.ContainsKey(applicationName))
            {
                return imagesCache_[applicationName];    
            }            
            return 0;
        }

        public void addImage(string applicationName, Icon image)
        {
            if (imagesCache_.ContainsKey(applicationName))
            {
                return;
            }
            imagesCache_.Add(applicationName, imageList_.Images.Count);
            imageList_.Images.Add(image);
        }

        public Image getImage(string applicationName)
        {
            return imageList_.Images[getImageIndex(applicationName)];
        }

        public void init(Dictionary<string, Icon> applicationsImages)
        {
            foreach (string applicationName in applicationsImages.Keys)
            {
                addImage(applicationName, applicationsImages[applicationName]);
            }
        }

        private ImageList imageList_ = new ImageList();
        private Dictionary<string, Int32> imagesCache_ = new Dictionary<string, Int32>();        
    }
}
