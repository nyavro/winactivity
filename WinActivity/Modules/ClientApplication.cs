using System;
using System.Collections.Generic;
using System.Text;
using WinActivityApplication.Events;
using WinActivityApplication.Controller;
using WinActivityApplication.Model;
using System.Threading;

namespace WinActivityApplication.Modules
{
    public class ClientApplication : IClientApplication
    {
        public ClientApplication(IServerApplication serverApplication)
        {
            serverApplication_ = serverApplication;
            create();
        }

        public void create()
        {            
            IModel model = new EventsModel(DBManager.getReader());
            processor_ = new EventsProcessor(model);
            
            //serverApplication_.getProxy().addObserver(processor_);

            controller_ = new EventsController(model, serverApplication_.getProxy(), processor_);

            Thread guiDynamicUpdaterThread = GuiDynamicUpdater.create(lifetimeProvider_, controller_);
            guiDynamicUpdaterThread.Start(); 
        }

        public void destroy()
        {
            serverApplication_.getProxy().removeObserver(processor_);
            lifetimeProvider_.setAlive(false);
        }

        public IController getController()
        {
            return controller_;
        }

        IServerApplication serverApplication_;
        IEventsProcessor processor_;
        ILifetimeProvider lifetimeProvider_ = new LifetimeProvider();
        IController controller_;
    }
}
