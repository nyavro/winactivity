using System;
using System.Collections.Generic;
using System.Text;
using WinActivityApplication.Events;

namespace WinActivityApplication.Modules
{
    public interface IServerApplication
    {
        IEventsProcessorProxy getProxy();
        void destroy();
    }
}
