using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using WinActivityApplication.Events;

namespace WinActivityApplication.Modules
{
    public class ServerApplication : IServerApplication
    {
        public ServerApplication()
        {
            init();
        }

        public void init()
        {
            proxy_ = new EventsProcessorProxy();

            IEventsProcessor buffer = new EventsBuffer(DBManager.getWriter());
            proxy_.addObserver(buffer);

            ApplicationsDBCache cache = new ApplicationsDBCache();
            Thread eventsGeneratorThread = EventsGenerator.create(lifetimeProvider_, cache, proxy_); 
            eventsGeneratorThread.Start();
        }        

        public void destroy()
        {
            lifetimeProvider_.setAlive(false);
        }

        public IEventsProcessorProxy getProxy()
        {
            return proxy_;
        }

        private IEventsProcessorProxy proxy_;
        private ILifetimeProvider lifetimeProvider_ = new LifetimeProvider();
    }
}
