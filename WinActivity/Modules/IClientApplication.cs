using System;
using System.Collections.Generic;
using System.Text;
using WinActivityApplication.Controller;

namespace WinActivityApplication.Modules
{
    public interface IClientApplication
    {
        void create();
        void destroy();
        IController getController();
    }
}
