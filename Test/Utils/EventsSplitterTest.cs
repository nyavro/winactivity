using System;
using System.Collections.Generic;
using System.Text;
using WinActivityApplication;
using WinActivityApplication.Database;
using NUnit.Framework;
using WinActivityApplication.utils;

namespace Test.Utils
{
    [TestFixture]
    public class EventsSplitterTest
    {
        [Test]
        public void testSplit()
        {
            EventsSplitter splitter = new EventsSplitter(null);
            LinkedList<IEvent> events = new LinkedList<IEvent>();
            splitter.split(new ActivityEvent("TestABC", "test", new DateTime(2011, 4, 19, 22, 12, 10), new TimeSpan(4, 10, 11)), events);

            Assert.AreEqual(2, events.Count);

            Assert.AreEqual(new DateTime(2011, 4, 19, 22, 12, 10), events.First.Value.getStart());
            Assert.AreEqual(new DateTime(2011, 4, 19, 23, 59, 59, 999), events.First.Value.getStart() + events.First.Value.getDuration());
            Assert.AreEqual(new DateTime(2011, 4, 20, 0, 0, 0), events.Last.Value.getStart());
            Assert.AreEqual(new DateTime(2011, 4, 20, 2, 22, 21), events.Last.Value.getStart() + events.Last.Value.getDuration());            
        }

        [Test]
        public void testSplit2()
        {
            EventsSplitter splitter = new EventsSplitter(null);
            LinkedList<IEvent> events = new LinkedList<IEvent>();
            splitter.split(new ActivityEvent("TestABC", "test", new DateTime(2011, 4, 19, 22, 12, 10), new TimeSpan(28, 10, 11)), events);

            Assert.AreEqual(3, events.Count);

            Assert.AreEqual(new DateTime(2011, 4, 19, 22, 12, 10), events.First.Value.getStart());
            Assert.AreEqual(new DateTime(2011, 4, 19, 23, 59, 59, 999), events.First.Value.getStart() + events.First.Value.getDuration());

            events.RemoveFirst();
            Assert.AreEqual(new DateTime(2011, 4, 20, 00, 00, 00), events.First.Value.getStart());
            Assert.AreEqual(new DateTime(2011, 4, 20, 23, 59, 59, 999), events.First.Value.getStart() + events.First.Value.getDuration());

            Assert.AreEqual(new DateTime(2011, 4, 21, 0, 0, 0), events.Last.Value.getStart());
            Assert.AreEqual(new DateTime(2011, 4, 21, 2, 22, 21), events.Last.Value.getStart() + events.Last.Value.getDuration());
        }
    }
}
