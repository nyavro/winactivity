using System;
using System.Collections.Generic;
using System.Text;
using NUnit.Framework;
using WinActivityApplication;

namespace Test.Events
{
    [TestFixture]
    public class AbstractEventTest
    {
        [Test]
        public void testClone()
        {
            IEvent testEvent = new ActivityEvent("testApp", "test", new DateTime(1980, 2, 1), new TimeSpan(1, 2, 3));
            Assert.AreEqual(1980, testEvent.getStart().Year);

            IEvent clone = testEvent.Clone();
            Assert.NotNull(clone);
            Assert.AreEqual("testApp", clone.getApplicationName());
            Assert.AreEqual("test", clone.getTitle());
            Assert.AreEqual(1980, clone.getStart().Year);
            Assert.AreEqual(2, clone.getStart().Month);
            Assert.AreEqual(1, clone.getStart().Day);
        }
        [Test]
        public void testSetStart()
        {
            IEvent testEvent = new ActivityEvent("testApp", "test", new DateTime(1980, 2, 1), new TimeSpan(1, 2, 3));
            testEvent.setStart(testEvent.getStart().AddMinutes(2));
            testEvent.setEnd(new DateTime(1980, 2, 1) + new TimeSpan(1, 2, 3));
            Assert.AreEqual(new TimeSpan(1, 0, 3), testEvent.getDuration());
        }
    }
}
