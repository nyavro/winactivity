using System;
using System.Collections.Generic;
using System.Text;
using WinActivityApplication;
using NUnit.Framework;
using WinActivityApplication.Database;

namespace Test.Database
{
    [TestFixture]
    public class DBWriterTest
    {        
        [Test]
        public void testDBWrite()
        {
            DBManager.initialize("Data Source=ev.db;Version=3;New=True;Compress=True;");
            IDBWriter writer = DBManager.getWriter();
            Assert.NotNull(writer);

            IEvent newEvent = new ActivityEvent("TestApp", "test", new DateTime(1980, 3, 4), new TimeSpan(1, 2, 3));
            writer.save(newEvent);
            
            IDBReader reader = DBManager.getReader();
            LinkedList<IEvent> events = reader.getEventsByDateRange(new DateRange(new DateTime(1970,1,1), new DateTime(1990, 1,1)));
            Assert.AreEqual(1, events.Count);
            Assert.AreEqual(new DateTime(1980,3,4), events.First.Value.getStart());
            Assert.AreEqual("TestApp", events.First.Value.getApplicationName());
            Assert.AreEqual("test", events.First.Value.getTitle());
            Assert.AreEqual(new TimeSpan(1, 2, 3), events.First.Value.getDuration());
            DBManager.close();
        }

        [Test]
        public void testRemove()
        {
            DBManager.initialize("Data Source=ev.db;Version=3;New=True;Compress=True;");
            IDBWriter writer = DBManager.getWriter();
            Assert.NotNull(writer);

            IEvent newEvent = new ActivityEvent("TestApp", "test", new DateTime(1980, 3, 4), new TimeSpan(1, 2, 3));
            writer.save(newEvent);

            IDBReader reader = DBManager.getReader();
            LinkedList<IEvent> events = reader.getEventsByDateRange(new DateRange(new DateTime(1970, 1, 1), new DateTime(1990, 1, 1)));
            Assert.AreEqual(1, events.Count);
            writer.save(newEvent);
            writer.remove(newEvent);
            LinkedList<IEvent> events2 = reader.getEventsByDateRange(new DateRange(new DateTime(1970, 1, 1), new DateTime(1990, 1, 1)));
            Assert.AreEqual(0, events2.Count);
            DBManager.close();
        }
    }
}
